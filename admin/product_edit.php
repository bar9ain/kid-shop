<?php include "header.php";

if (isset($_POST["masanpham"])) {
    $masanpham = $_POST["masanpham"];
    $tensanpham = $_POST["tensanpham"];
    $madanhmuc = $_POST["madanhmuc"];
    $gia = $_POST["gia"];
    $soluong = $_POST["soluong"];
    $thuonghieu = $_POST["thuonghieu"];
    $chatlieu = $_POST["chatlieu"];
    $xuatxu = $_POST["xuatxu"];
    if (isset($_FILES["hinhanh"]) && $_FILES["hinhanh"]["size"] > 0) {
        $time = time();
        $type = explode("/", $_FILES["hinhanh"]["type"])[1];
        $hinhanh = "images/$time.$type";
        move_uploaded_file($_FILES["hinhanh"]["tmp_name"], "../" . $hinhanh);
        $sql = "UPDATE sanpham
                SET
                    tensanpham='$tensanpham',
                    madanhmuc='$madanhmuc',
                    gia='$gia',
                    soluong='$soluong',
                    thuonghieu='$thuonghieu',
                    chatlieu='$chatlieu',
                    xuatxu='$xuatxu',
                    hinhanh='$hinhanh'
                WHERE masanpham='$masanpham'";
    } else {
        $sql = "UPDATE sanpham
                SET
                    tensanpham='$tensanpham',
                    madanhmuc='$madanhmuc',
                    gia='$gia',
                    soluong='$soluong',
                    thuonghieu='$thuonghieu',
                    chatlieu='$chatlieu',
                    xuatxu='$xuatxu'
                WHERE masanpham='$masanpham'";
    }
    if ($db->query($sql)) {
        header("Location: product_list.php");
    }
}
if (isset($_GET["id"])) {
    $masanpham = $_GET["id"];
    $sql = "select * from sanpham where masanpham='$masanpham'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: product_list.php");
    }
} else {
    header("Location: product_list.php");
}

$sql = "select * from danhmucsanpham";
$query = $db->query($sql);
$list = array();
while ($item = $query->fetch_array()) {
    $list[] = $item;
}

?>

    <form method="post" enctype="multipart/form-data">
        <div class="edit-page">
            <div class="edit-header">
                <div class="edit-header-title">Cập nhật sản phẩm</div>
                <div class="edit-header-subtitle">Update product</div>
            </div>
            <div class="edit-body">
                <div class="edit-body-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Mã sản phẩm</div>
                        <div class="input-with-label-content">
                            <input type="text" name="masanpham" class="input-with-status-input" readonly
                                   value="<?= $row["masanpham"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên sản phẩm</div>
                        <div class="input-with-label-content">
                            <input type="text" name="tensanpham" class="input-with-status-input" required autofocus
                                   value="<?= $row["tensanpham"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Danh mục sản phẩm</div>
                        <div class="input-with-label-content">
                            <select class="input-with-status-input" name="madanhmuc">
                                <?php foreach ($list as $item) { ?>
                                    <option value="<?= $item["madanhmuc"] ?>"
                                        <?= $item["madanhmuc"] == $row["madanhmuc"] ? "selected" : "" ?>>
                                        <?= $item["tendanhmuc"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Giá</div>
                        <div class="input-with-label-content">
                            <input type="number" name="gia" class="input-with-status-input" required
                                   value="<?= $row["gia"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Số lượng</div>
                        <div class="input-with-label-content">
                            <input type="number" name="soluong" class="input-with-status-input" required
                                   value="<?= $row["soluong"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Thương hiệu</div>
                        <div class="input-with-label-content">
                            <input type="text" name="thuonghieu" class="input-with-status-input"
                                   value="<?= $row["thuonghieu"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Chất liệu</div>
                        <div class="input-with-label-content">
                            <input type="text" name="chatlieu" class="input-with-status-input"
                                   value="<?= $row["chatlieu"] ?>">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Xuất xứ</div>
                        <div class="input-with-label-content">
                            <input type="text" name="xuatxu" class="input-with-status-input"
                                   value="<?= $row["xuatxu"] ?>">
                        </div>
                    </div>
                    <div class="edit-submit">
                        <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                        <a href="product_list.php" class="btn-cancel">Trở lại</a>
                    </div>
                </div>
                <div class="edit-body-right">
                    <div class="edit-thumbnail">
                        <div class="edit-thumbnail-image">
                            <img src="../<?= $row["hinhanh"] ?>">
                        </div>
                        <input type="file" accept="image/*" name="hinhanh">
                    </div>
                </div>
            </div>
        </div>
    </form>

<?php include "footer.php"; ?>