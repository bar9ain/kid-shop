<?php include "header.php";
$sql = "SELECT
            donhang.madonhang,
            donhang.tongtien,
            donhang.ngaythem,
            donhang.trangthai,
            khachhang.hoten
        FROM donhang
        LEFT JOIN khachhang ON donhang.makhachhang = khachhang.makhachhang
        ORDER BY trangthai, ngaythem DESC";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<div class="card flex-column">
    <div class="card-header">
        <div class="card-header-title">Danh sách đơn hàng</div>
    </div>
    <div class="card-body">
        <table class="admin-table">
            <tr class="admin-table-row">
                <th class="admin-table-header">Mã đơn hàng</th>
                <th class="admin-table-header">Khách hàng</th>
                <th class="admin-table-header">Tổng tiền</th>
                <th class="admin-table-header">Thời gian</th>
                <th class="admin-table-header">Trạng thái</th>
                <th class="admin-table-header">Chức năng</th>
            </tr>
            <?php foreach ($list as $item) { ?>
                <tr class="admin-table-row">
                    <td class="admin-table-data"><?= $item["madonhang"] ?></td>
                    <td class="admin-table-data"><?= $item["hoten"] ?></td>
                    <td class="admin-table-data"><?= number_format($item["tongtien"]) ?>đ</td>
                    <td class="admin-table-data"><?= $item["ngaythem"] ?></td>
                    <td class="admin-table-data">
                        <?php
                        switch ($item["trangthai"]) {
                            case 0:
                                echo "Mới";
                                break;
                            case 1:
                                echo "Đã xác nhận";
                                break;
                            case 2:
                                echo "Đã giao hàng";
                                break;
                            case 3:
                                echo "Đã nhận hàng";
                                break;
                            case 4:
                                echo "Đã hủy";
                                break;
                        }
                        ?>
                    </td>
                    <td class="admin-table-data">
                        <a href="order_detail.php?id=<?= $item["madonhang"] ?>">Chi tiết</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<?php include "footer.php" ?>
