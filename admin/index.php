<?php include "../csdl.php";
if (session_status() === PHP_SESSION_NONE)
    session_start();

if (isset($_SESSION["admin"])) {
    header("Location: admin_list.php");
}

if (isset($_POST["login"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $matkhau = $_POST["matkhau"];
    $sql = "SELECT tendangnhap
            FROM admin
            WHERE tendangnhap='$tendangnhap'
            AND matkhau='$matkhau'";
    $check = $db->query($sql);
    if ($check->num_rows > 0) {
        $_SESSION["admin"] = $tendangnhap;
        header("Location: admin_list.php");
    } else {
        $login_error = true;
    }
}

?>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <link href="../css/bootstrap.min.css" rel=" stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    <link rel="icon" href="../images/coin.png">
</head>

<body>
<div class="page-content">
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Đăng nhập</a>
            </div>
            <div class="authen-body">
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="tendangnhap"
                           placeholder="Tên đăng nhập"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="password"
                           name="matkhau"
                           placeholder="Mật khẩu"
                           required>
                </div>
                <?php if (isset($login_error)) { ?>
                    <div class="authen-error">Tài khoản hoặc mật khẩu không đúng</div>
                <?php } ?>
                <div class="forgot-password">
                    <a href="forgot_password.php">Quên mật khẩu</a>
                </div>
            </div>
            <div class="authen-footer">
                <button type="submit" name="login" class="btn btn-submit">
                    Đăng nhập
                </button>
            </div>
        </div>
    </form>
</div>
</body>