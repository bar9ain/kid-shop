<?php include "header.php";

if (isset($_POST["tendanhmuc"])) {
    $tendanhmuc = $_POST["tendanhmuc"];
    $hienthitrangchu = isset($_POST["hienthitrangchu"]) ? 1 : 0;
    $hienthimenu = isset($_POST["hienthimenu"]) ? 1 : 0;
    if (isset($_FILES["hinhanh"]) && $_FILES["hinhanh"]["size"] > 0) {
        $time = time();
        $type = explode("/", $_FILES["hinhanh"]["type"])[1];
        $hinhanh = "images/$time.$type";
        move_uploaded_file($_FILES["hinhanh"]["tmp_name"], "../" . $hinhanh);
    }
    $sql = "INSERT INTO danhmucsanpham(
                tendanhmuc,
                hinhanhdanhmuc,
                hienthitrangchu,
                hienthimenu
            ) VALUES (
                '$tendanhmuc',
                '$hinhanh',
                '$hienthitrangchu',
                '$hienthimenu'
            )";
    if ($db->query($sql)) {
        header("Location: category_list.php");
    }
}

?>

    <form method="post" enctype="multipart/form-data">
        <div class="edit-page">
            <div class="edit-header">
                <div class="edit-header-title">Tạo mới danh mục</div>
                <div class="edit-header-subtitle">Add new category</div>
            </div>
            <div class="edit-body">
                <div class="edit-body-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên danh mục</div>
                        <div class="input-with-label-content">
                            <input type="text" name="tendanhmuc" class="input-with-status-input" required autofocus>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Hiển thị ở trang chủ</div>
                        <div class="input-with-label-content">
                            <input id="homepage" type="checkbox" name="hienthitrangchu" checked>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Hiển thị ở menu</div>
                        <div class="input-with-label-content">
                            <input id="menu" type="checkbox" name="hienthimenu" checked>
                        </div>
                    </div>
                    <div class="edit-submit">
                        <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                        <a href="category_list.php" class="btn-cancel">Trở lại</a>
                    </div>
                </div>
                <div class="edit-body-right">
                    <div class="edit-thumbnail">
                        <div class="edit-thumbnail-image">
                            <img src="../images/empty.png">
                        </div>
                        <input type="file" accept="image/*" name="hinhanh" required>
                    </div>
                </div>
            </div>
        </div>
    </form>

<?php include "footer.php"; ?>