<?php
include "header.php";
if (!isset($_SESSION["makhachhang"])) {
    header("location: dangnhap.php");
}

$makhachhang = $_SESSION["makhachhang"];

if (isset($_GET["id"])) {
    $madonhang = $_GET["id"];
    $sql = "SELECT
            donhang.madonhang,
            donhang.tongtien,
            donhang.ngaythem,
            donhang.trangthai
        FROM donhang
        LEFT JOIN khachhang ON donhang.makhachhang = khachhang.makhachhang
        WHERE donhang.madonhang = '$madonhang'
        AND donhang.makhachhang = '$makhachhang'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: donhang.php");
    } else {
        $madonhang = $row["madonhang"];
        $thoigian = $row["ngaythem"];
        $tongtien = $row["tongtien"];
        $trangthai = $row["trangthai"];
    }
    $sql = "SELECT
                sanpham.masanpham,
                sanpham.hinhanh,
                chitietdonhang.tensanpham,
                chitietdonhang.dongia,
                chitietdonhang.soluong
            FROM chitietdonhang
            LEFT JOIN sanpham ON chitietdonhang.masanpham = sanpham.masanpham
            WHERE madonhang = '$madonhang'";
    $result = $db->query($sql);
    $list = array();
    while ($i = $result->fetch_array()) {
        $list[] = $i;
    }
} else {
    header("Location: donhang.php");
}

?>
<div class="user-page">
    <?php include "hoso_menu.php" ?>
    <div class="user-page-content">
        <div class="user-page-header">
            <div class="user-page-header-title">Chi tiết đơn hàng</div>
            <div class="user-page-header-subtitle">Mã đơn hàng: <?= $madonhang ?>
                | Thời gian: <?= $thoigian ?></div>
        </div>
        <div class="edit-body">
            <div class="edit-body-cart">
                <?php foreach ($list as $item) { ?>
                    <div class="cart-detail-row">
                        <div class="cart-detail-image">
                            <img src="<?= $item["hinhanh"] ?>">
                        </div>
                        <div class="cart-detail-text">
                            <div class="cart-detail-title"><?= $item["tensanpham"] ?></div>
                            <div class="cart-detail-quantity">x<?= $item["soluong"] ?></div>
                        </div>
                        <div class="cart-detail-price"><?= number_format($item["dongia"]) ?>đ</div>
                    </div>
                <?php } ?>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Tổng tiền hàng</div>
                    <div class="cart-detail-summary-value">
                        <span><?= number_format($tongtien) ?>đ</span>
                    </div>
                </div>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Trạng thái</div>
                    <div class="cart-detail-summary-value">
                        <?php
                        switch ($trangthai) {
                            case 0:
                                echo "Mới";
                                break;
                            case 1:
                                echo "Đã xác nhận";
                                break;
                            case 2:
                                echo "Đã giao hàng";
                                break;
                            case 3:
                                echo "Đã nhận hàng";
                                break;
                            case 4:
                                echo "Đã hủy";
                                break;
                        }
                        ?>
                    </div>
                </div>
                <div class="edit-submit">
                    <a href="donhang.php" class="btn-cancel">Trở lại</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
