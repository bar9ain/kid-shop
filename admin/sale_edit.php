<?php
include "header.php";

if (isset($_POST["save"])) {
    $makhuyenmai = $_POST["save"];
    $giatri = $_POST["giatri"];
    $batdau = $_POST["thoigianbatdau"];
    $ketthuc = $_POST["thoigianketthuc"];
    if ($batdau > $ketthuc) {
        $alert = "Ngày bắt đầu phải nhỏ hơn ngày kết thúc!";
    } else {
        $sql = "UPDATE khuyenmai
                SET giatri = '$giatri',
                    thoigianbatdau = '$batdau',
                    thoigianketthuc = '$ketthuc'
                WHERE makhuyenmai = '$makhuyenmai'";
        if ($db->query($sql)) {
            header("location: sale_list.php");
        } else echo $sql;
    }
}

if (isset($_GET["id"])) {
    $makhuyenmai = $_GET["id"];
    $sql = "select
                sanpham.masanpham,
                sanpham.tensanpham,
                sanpham.gia,
                khuyenmai.makhuyenmai,
                khuyenmai.giatri,
                khuyenmai.thoigianbatdau,
                khuyenmai.thoigianketthuc
            from khuyenmai
            left join sanpham on khuyenmai.masanpham = sanpham.masanpham
            where makhuyenmai='$makhuyenmai'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: sale_list.php");
    }
} else {
    header("Location: sale_list.php");
}
?>

<form method="post">
    <div class="edit-page">
        <div class="edit-header">
            <div class="edit-header-title">Tạo khuyến mãi</div>
            <div class="edit-header-subtitle">Create sale</div>
        </div>
        <div class="edit-body">
            <div class="edit-body-left">
                <div class="input-with-label">
                    <div class="input-with-label-label">Mã sản phẩm</div>
                    <div class="input-with-label-content">
                        <input type="text" name="masanpham" class="input-with-status-input"
                               value="<?= $row["masanpham"] ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Tên sản phẩm</div>
                    <div class="input-with-label-content">
                        <input type="text" name="tendanhmuc" class="input-with-status-input"
                               value="<?= $row["tensanpham"] ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Giá</div>
                    <div class="input-with-label-content">
                        <input type="text" name="gia" class="input-with-status-input"
                               value="<?= number_format($row["gia"]) ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Giảm giá (%)</div>
                    <div class="input-with-label-content">
                        <input type="number" name="giatri" class="input-with-status-input"
                               value="<?= $row["giatri"] ?>" autofocus required max="100" min="0">
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Ngày bắt đầu</div>
                    <div class="input-with-label-content">
                        <input type="date" class="input-with-status-input" name="thoigianbatdau"
                               value="<?= $row["thoigianbatdau"] ?>" required>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Ngày kết thúc</div>
                    <div class="input-with-label-content">
                        <input type="date" class="input-with-status-input" name="thoigianketthuc"
                               value="<?= $row["thoigianketthuc"] ?>" required>
                    </div>
                </div>
                <div class="edit-submit">
                    <button type="submit" name="save" class="btn btn-submit" value="<?= $row["makhuyenmai"] ?>">Lưu
                    </button>
                    <a href="sale_list.php" class="btn-cancel">Trở lại</a>
                </div>
            </div>
        </div>
    </div>
</form>

<?php include "footer.php" ?>
