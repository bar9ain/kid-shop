-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 16, 2019 at 04:49 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `tendangnhap` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `matkhau` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `reset` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tendangnhap`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`tendangnhap`, `matkhau`, `reset`) VALUES
('admin', '123', 0),
('abc', '1', 0),
('admin3', '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `binhluan`
--

DROP TABLE IF EXISTS `binhluan`;
CREATE TABLE IF NOT EXISTS `binhluan` (
  `mabinhluan` int(11) NOT NULL AUTO_INCREMENT,
  `makhachhang` int(11) NOT NULL,
  `masanpham` int(11) NOT NULL,
  `noidung` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL,
  `thoigian` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `trangthai` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mabinhluan`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `binhluan`
--

INSERT INTO `binhluan` (`mabinhluan`, `makhachhang`, `masanpham`, `noidung`, `thoigian`, `trangthai`) VALUES
(1, 1, 1357053224, 'Giao hàng rất nhanh', '2019-05-16 23:03:57', 1),
(2, 1, 1357053224, 'asd', '2019-05-16 23:10:07', 1),
(3, 2, 1300903249, 'Wonderful', '2019-05-16 23:39:52', 0),
(4, 2, 721912125, 'Rẻ và đẹp!', '2019-05-16 23:41:26', 0),
(5, 2, 425407018, '♥♥♥', '2019-05-16 23:48:55', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chitietdonhang`
--

DROP TABLE IF EXISTS `chitietdonhang`;
CREATE TABLE IF NOT EXISTS `chitietdonhang` (
  `madonhang` int(11) NOT NULL,
  `masanpham` int(11) NOT NULL,
  `tensanpham` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `dongia` bigint(20) NOT NULL,
  `soluong` int(11) NOT NULL,
  PRIMARY KEY (`madonhang`,`masanpham`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `chitietdonhang`
--

INSERT INTO `chitietdonhang` (`madonhang`, `masanpham`, `tensanpham`, `dongia`, `soluong`) VALUES
(1556177931, 203302843, '(Tặng tranh ghép gỗ)BỘ ĐỒ CHƠI BẰNG GỖ THẢ HÌNH THEO CHỦ ĐỀ', 112500, 1),
(1556178122, 203302843, '(Tặng tranh ghép gỗ)BỘ ĐỒ CHƠI BẰNG GỖ THẢ HÌNH THEO CHỦ ĐỀ', 112500, 4),
(1556181481, 1357053224, 'Đồng hồ gỗ đồ chơi cho trẻ em', 110000, 1),
(1556183231, 1300903249, 'Mô Hình Lắp Ráp Gundam Bandai HGBD 009 Gundam 00 Diver Ace', 899000, 1),
(1556355929, 721912125, 'Đồ chơi luồn hạt gỗ', 39200, 4),
(1556355929, 1357053224, 'Đồng hồ gỗ đồ chơi cho trẻ em', 110000, 1),
(1558011982, 203302843, '(Tặng tranh ghép gỗ)BỘ ĐỒ CHƠI BẰNG GỖ THẢ HÌNH THEO CHỦ ĐỀ', 123750, 1);

-- --------------------------------------------------------

--
-- Table structure for table `danhmucsanpham`
--

DROP TABLE IF EXISTS `danhmucsanpham`;
CREATE TABLE IF NOT EXISTS `danhmucsanpham` (
  `madanhmuc` int(11) NOT NULL AUTO_INCREMENT,
  `tendanhmuc` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `hinhanhdanhmuc` text COLLATE utf8_vietnamese_ci NOT NULL,
  `hienthitrangchu` int(11) NOT NULL DEFAULT '1',
  `hienthimenu` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`madanhmuc`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `danhmucsanpham`
--

INSERT INTO `danhmucsanpham` (`madanhmuc`, `tendanhmuc`, `hinhanhdanhmuc`, `hienthitrangchu`, `hienthimenu`) VALUES
(1, 'Đồ chơi gỗ', 'images/wood.png', 1, 1),
(2, 'Búp bê, gấu bông', 'images/wool.png', 1, 1),
(3, 'Đồ chơi lắp ghép', 'images/lego.png', 1, 1),
(4, 'Đồ chơi giải trí', 'images/board.png', 1, 1),
(5, 'Đồ chơi thương hiệu', 'images/brand.png', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `donhang`
--

DROP TABLE IF EXISTS `donhang`;
CREATE TABLE IF NOT EXISTS `donhang` (
  `madonhang` int(11) NOT NULL AUTO_INCREMENT,
  `tongtien` bigint(20) NOT NULL,
  `ngaythem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `makhachhang` int(11) NOT NULL,
  `trangthai` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`madonhang`)
) ENGINE=InnoDB AUTO_INCREMENT=1558011983 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `donhang`
--

INSERT INTO `donhang` (`madonhang`, `tongtien`, `ngaythem`, `makhachhang`, `trangthai`) VALUES
(1556177931, 112500, '2019-04-25 16:38:51', 1, 2),
(1556178122, 450000, '2019-04-25 16:42:02', 1, 4),
(1556181481, 110000, '2019-04-25 17:38:01', 1, 3),
(1556183231, 899000, '2019-04-25 18:07:11', 1, 0),
(1556355929, 266800, '2019-04-27 16:05:29', 1, 2),
(1558011982, 123750, '2019-05-16 20:06:22', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

DROP TABLE IF EXISTS `khachhang`;
CREATE TABLE IF NOT EXISTS `khachhang` (
  `makhachhang` int(11) NOT NULL AUTO_INCREMENT,
  `hoten` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `diachi` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `sdt` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `trangthai` int(11) NOT NULL DEFAULT '1',
  `avatar` text COLLATE utf8_vietnamese_ci,
  `email` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  PRIMARY KEY (`makhachhang`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `sdt` (`sdt`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`makhachhang`, `hoten`, `diachi`, `sdt`, `trangthai`, `avatar`, `email`, `password`) VALUES
(1, 'Nguyễn Văn An', 'TP Vinh, Nghệ An', '0123456789', 1, 'images/1556207243.jpg', 'abc@gmail.com', '123'),
(2, 'Nguyễn Thị Vân', 'Anh Sơn, Nghệ An', '0123545455', 1, NULL, 'aaa@aaa.com', '123'),
(30, 'Trịnh Đình Bầu', 'Thanh Hóa', '0123456777', 1, NULL, 'bbb@bbb.com', '123');

-- --------------------------------------------------------

--
-- Table structure for table `khuyenmai`
--

DROP TABLE IF EXISTS `khuyenmai`;
CREATE TABLE IF NOT EXISTS `khuyenmai` (
  `makhuyenmai` int(11) NOT NULL AUTO_INCREMENT,
  `masanpham` int(11) NOT NULL,
  `thoigianbatdau` date NOT NULL,
  `thoigianketthuc` date NOT NULL,
  `giatri` int(11) NOT NULL,
  PRIMARY KEY (`makhuyenmai`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `khuyenmai`
--

INSERT INTO `khuyenmai` (`makhuyenmai`, `masanpham`, `thoigianbatdau`, `thoigianketthuc`, `giatri`) VALUES
(1, 203302843, '2019-04-16', '2019-04-29', 45),
(3, 721912125, '2019-04-12', '2019-04-28', 20);

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

DROP TABLE IF EXISTS `sanpham`;
CREATE TABLE IF NOT EXISTS `sanpham` (
  `masanpham` int(11) NOT NULL AUTO_INCREMENT,
  `tensanpham` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `gia` bigint(20) NOT NULL,
  `hinhanh` text COLLATE utf8_vietnamese_ci NOT NULL,
  `madanhmuc` int(11) NOT NULL DEFAULT '1',
  `soluong` int(11) NOT NULL,
  `thuonghieu` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `chatlieu` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `xuatxu` varchar(100) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `thoigian` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `luotxem` int(11) NOT NULL DEFAULT '0',
  `luotmua` int(11) NOT NULL DEFAULT '0',
  `ngaythem` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`masanpham`)
) ENGINE=MyISAM AUTO_INCREMENT=1732315141 DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`masanpham`, `tensanpham`, `gia`, `hinhanh`, `madanhmuc`, `soluong`, `thuonghieu`, `chatlieu`, `xuatxu`, `thoigian`, `luotxem`, `luotmua`, `ngaythem`) VALUES
(203302843, '(Tặng tranh ghép gỗ)BỘ ĐỒ CHƠI BẰNG GỖ THẢ HÌNH THEO CHỦ ĐỀ', 225000, 'images/b6ad9d589faf02c289a6b9bb9a505cc7.jpg', 1, 211, '', '', '', '2019-04-23 12:26:25', 28, 5, '2019-04-27 11:24:50'),
(1357053224, 'Đồng hồ gỗ đồ chơi cho trẻ em', 110000, 'images/4570893847ce79e2c5c1b261b77382c2.jpg', 1, 24, NULL, '', '', '2019-04-23 12:26:25', 107, 2, '2019-04-27 11:24:50'),
(394895091, 'Bảng ghép hình bằng gỗ - Chữ cái in hoa', 65000, 'images/5f88330e7641201de764e1684cec1381.jpg', 1, 46, NULL, 'Gỗ', '', '2019-04-23 12:26:25', 12, 0, '2019-04-27 11:24:50'),
(168755415, 'Bộ đồ chơi rút gỗ 54 thanh', 50000, 'images/9cf46f23ae4e47d5f39b055a70025420.jpg', 1, 277, '', 'Gỗ', '', '2019-04-23 12:26:25', 7, 0, '2019-04-27 11:24:50'),
(433377632, 'Bảng 2 mặt kèm bộ chữ số bằng gỗ gắn nam châm', 150000, 'images/d6a43000c0428e9d90fd79bf939addbf.jpg', 1, 16, 'Khác', 'Gỗ', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(724757059, 'Bộ 10 Tranh Ghép Gỗ 3D Động Vật Hoa Quả Phương Tiện Giao Thông', 160000, 'images/143e6d81ff3b90025f4b84fba561b154.jpg', 1, 456, '', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(203316733, 'BỘ GHÉP MÔ HÌNH GIAO THÔNG THÀNH PHỐ BẰNG GỖ', 259000, 'images/dd399b720c37e739a34cd8aee30c9714.jpg', 1, 411, '', 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1246765644, 'Bộ Luyện Tay Luồn Cọc Bằng Gỗ', 229000, 'images/11d16b7c87a1152cf89c4a8a6f7a52e7.jpg', 1, 341, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1396583215, 'Đồ Chơi Gỗ Xe Kéo Luồn Hạt Giúp Bé Phát Triển Tư Duy', 159000, 'images/842cddb5410cb5203406b83c29967ce4.jpg', 1, 456, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 4, 0, '2019-04-27 11:24:50'),
(833447444, 'Bảng Số Nổi Bằng Gỗ Cao Cấp', 129000, 'images/5aa9a5febe40fd501347aba5a3a235c9.jpg', 1, 457, NULL, '', '', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1484210994, 'Đồ Chơi Xếp Hình Khối Bằng Gỗ 75 Chi Tiết Giúp Bé Phát Triển Trí Não', 265000, 'images/76ec81ffbfe386e6097277d8165f9fda.jpg', 1, 494, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 5, 0, '2019-04-27 11:24:50'),
(465561329, 'Đoàn tàu số - đồ chơi giáo dục gỗ an toàn (loại đẹp)', 110000, 'images/3c4075159378c6bd92f5cac1d4ddc0f9.jpg', 1, 92, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1303754700, 'Đồ Chơi Gỗ Hộp Học Đếm Và Ghép Số Cho Bé Hàng VN Tặng 40 Thẻ Học Chữ Cái Và Số', 259000, 'images/4f148a8369366ebc60cb5cb5bd4ed0f1.jpg', 1, 411, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(660861061, 'Bộ 2 Bảng Ghép Học Chữ Và Học Số Bằng Gỗ Cho Bé Tặng Tranh 3D', 119000, 'images/65c50d79e4c73d216ab85f09e66763f3.jpg', 1, 286, NULL, '', '', '2019-04-23 12:26:25', 5, 0, '2019-04-27 11:24:50'),
(501700460, 'Đồ Chơi Xếp Hình Khối Bằng Gỗ - Chữ Cái, Chữ Số Và Các Con Vật 100 Chi Tiết', 290000, 'images/234283e243d9c671cd6b5dbe8b8eaeed.jpg', 1, 384, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 4, 0, '2019-04-27 11:24:50'),
(728487655, 'Sách Ghép Gỗ Chủ Đề Động Vật Tự Nhiên(Họa Tiết Ngẫu Nhiên)', 99000, 'images/7e58bb09cc34e0bf3e7e35a0655ddf58.jpg', 1, 500, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(879748396, 'Bộ 3 Bảng Chữ Cái In Thường In Hoa Và Số Học Nổi Song Ngữ Bằng Gỗ Cao Cấp', 329000, 'images/aaca176549283df8009377f4cfdc852f.jpg', 1, 403, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(721933339, 'Tranh ghép gỗ nổi hình - Đồ chơi giáo dục cho bé', 25000, 'images/34173150cd4e967b4615caf30baca57e.jpg', 1, 253, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1122303366, 'Bảng 20 Số Nổi Bằng Gỗ', 145000, 'images/876a8cacddba84764a918072a70b4c19.jpg', 1, 479, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(465561338, 'Đồ chơi Hộp thả khối đa năng - Đồ chơi giáo dục gỗ an toàn', 195000, 'images/e2f16f09b5996409c99543d2135bd3bd.jpg', 1, 1221, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(465561372, 'Đồ chơi bộ xếp hình cắt bánh gato gỗ cho bé', 180000, 'images/ec029e14e947d3a42d928caebc1b2dc4.jpg', 1, 744, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1487415066, 'Đồ chơi cân gỗ cho bé', 220000, 'images/a86e9242e6bdf093f5df83f94e14b440.jpg', 1, 132, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(465561332, 'Đồ chơi tranh ghép gỗ 6 mặt - xếp hình 3D cho bé', 55000, 'images/72c1ed872709b01ca5f8cf22f800f427.jpg', 1, 1045, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(721912125, 'Đồ chơi luồn hạt gỗ', 49000, 'images/04834cd97f645ad4a2800585e6009c61.jpg', 1, 0, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 20, 4, '2019-04-27 11:24:50'),
(997173363, 'Hộp đựng bút bằng gỗ', 22858, 'images/24b1c3a36406c613c06edd068bcb685e.jpg', 1, 3, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1399677768, 'Bộ hộp toán học đa năng gỗ dày đẹp.', 125000, 'images/174547fa4ca7feca116436290aee21e7.jpg', 1, 3, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1280569779, 'Domino gỗ đẹp 100 chi tiết số từ 0-99', 200000, 'images/c9294646a9759e1dfbe91f18f7519196.jpg', 1, 1, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(617169725, 'Đồ chơi bảng núm gỗ loại 20x30 cm nhiều chủ đề', 55000, 'images/95fd2eac05536f341df9bf65e0ea14b7.jpg', 1, 2186, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1196403979, 'Bộ domino 28 miếng gỗ', 120000, 'images/2473837afe43feeeae16c739851b6c8d.jpg', 1, 3, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(724762000, 'Bộ 20 Tranh Ghép Gỗ Nổi Động Vật Hoa Quả Phương Tiện Giao Thông', 350000, 'images/86db67f6ed0871f4abc43772734215f3.jpg', 1, 853, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(846744075, 'Đồ chơi mô hình nhà gỗ DIY Cute Room H013( Tặng Mica + Keo)', 190000, 'images/df5e717bb81f76ac2504a2c7b5212757.jpg', 1, 12, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(982720798, 'bảng chữ cái và số nổi', 150000, 'images/04735708ea1ce3c5568a98d112708840.jpg', 1, 175, 'Bandai', 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(682887505, 'Đồ chơi gỗ Bộ luồn hạt sâu vòng chữ cái LH01 đồ chơi gỗ cho bé', 110000, 'images/141bfa4da839bce0af89058ea3540533.jpg', 1, 1, '', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(864780460, 'Đồ chơi gỗ cho bé đập cọc vui nhộn DB08', 100000, 'images/b153d878152f2a670992574128aa79f1.jpg', 1, 68, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(861446149, 'Ngựa bập bênh gỗ', 420000, 'images/4f318eca30ddbdc9ba7bbbd77363e584.jpg', 1, 46, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(705016131, 'Đồ chơi gỗ GCB - Bảng số nổi BA82', 145000, 'images/57bfd805047040b6895da3644375b6ac.jpg', 1, 8, 'Thương hiệu GCB', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(844423058, 'Nhà Búp Bê DIY- Happy kitchen H009 ( Tặng mica + keo )', 175000, 'images/24df104ec241567a99dd491804407ef8.jpg', 1, 19, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(682812452, 'Đập bóng hình các con vật DB02 đồ chơi gỗ cho bé', 130000, 'images/cdb775cd6bac325bfe91c1b6d65e5f71.jpg', 1, 185, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1106952641, 'Xúc xắc đầu gỗ tròn loại nhỏ cho bé yêu', 12000, 'images/32fd23149e83c222c3cae173f8d63ed7.jpg', 1, 72, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(679374352, 'Đồ chơi gỗ Bàn tính học toán BA01', 70000, 'images/6178e390fa3440570980396b03d545bf.jpg', 1, 65, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(691956657, 'Đồ chơi gỗ Bảng số nổi 1-20 BA85 đồ chơi gỗ cho bé', 140000, 'images/81f3d1d49bf7e7b7d155029bae7729ee.jpg', 1, 3, 'GCB', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(679493779, 'Đồ chơi gỗ Bảng ghép hình khối nổi BA87', 140000, 'images/70c6e1f7c1a171db0377030ff91cedd9.jpg', 1, 95, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(809123678, 'Đồ chơi gỗ cho bé luồn hạt thả hình vượt chướng ngại vật', 100000, 'images/0e2f6a1bb9a14a76ef910f6df537a43b.jpg', 1, 57, 'GCB', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(864773112, 'Đồ chơi gỗ cho bé xếp 4 hình học mầm non XH57', 100000, 'images/a7079b5ffb1cc3c645f4fe7f66c74c7a.jpg', 1, 76, 'Thương hiệu GCB', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1055433080, 'Đồ chơi gỗ - Đoàn tàu chở chữ và số', 189000, 'images/845e6c554147dd7a45fa7bc5de7b5d81.jpg', 1, 2, NULL, 'Gỗ', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(724219114, 'Gấu brown khổ vải 1m', 285000, 'images/bd4da4dec59bc342bd84d2056993e321.jpg', 2, 365, NULL, 'BÔNG', 'Việt Nam', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(979624403, 'KHỦNG LONG NHẬP KHẨU XỊN SIÊU CIU CHO BÉ (Giao màu ngẫu nhiên)', 109000, 'images/9b57da8f216ed40f14f351b69ab07d77.jpg', 2, 159, NULL, 'vải', '', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(724180412, 'Gấu bông teddy áo len khổ vải 1m2 (size thật 1m)', 299000, 'images/40ceeb1205f3bc6c8e42f393ca82f207.jpg', 2, 983, NULL, 'bông', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(198010065, '- Búp bê Baby Doll bình sữa: Có âm thanh cười, khóc, uống nước, đi vệ sinh (tè)', 250000, 'images/864e8512b6f8f663c113660329b47605.jpg', 2, 142, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1481846846, 'THÚ NHỒI BÔNG SIÊU NGỘ NGHĨNH ĐÁNG YÊU SIZE 23CM', 150000, 'images/74c7569cd1e1aafa222652bd437b9bc3.jpg', 2, 57, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(346101349, '- Búp bê nàng tiên cá: Có khớp (Nhiều màu)', 225000, 'images/92f8eda6bacaf127f2ca9de566b52d94.jpg', 2, 47, NULL, 'Nhựa', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1345942045, 'Thú bông MUMUSO hình con cừu đáng yêu', 350000, 'images/78618daae5c32ed50c4eb82c5e37fcc4.jpg', 2, 99, NULL, 'Vải', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1256555548, 'Vợt Mèo Vợt Côn Trùng Cho Bé Khi Tắm BMBE1029', 74000, 'images/66bcfc8fe04ec5ef3b05a2d3cac4655d.jpg', 2, 996, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(891661457, 'Thỏ hồng chân dài - hàng nhập - vải siêu mịn cho bé iêu', 119000, 'images/c6f2dbef4bbf3e6931dcc11b8823e246.jpg', 2, 110, NULL, 'Vải nhung mềm', 'Thái Lan', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(726453578, 'Gấu teddy choàng khăn cổ', 409000, 'images/c612afe1842ca0579b374f20c06064c4.jpg', 2, 47, NULL, 'bông', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1346030751, 'Thú bông MUMUSO hình gấu con đáng yêu', 200000, 'images/1b826520d7c66a54b4080a58e0082739.jpg', 2, 99, NULL, 'Vải', 'Trung Quốc', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1269525554, 'Chú Bò Bông Thư Kí Kim Size 70cm', 500000, 'images/9d251f5796d5e72b3f30b14a112c6bf8.jpg', 2, 35, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1544253058, 'Thú Bông MUMUSO Hình Khủng Long Hồng Cực Đẹp', 350000, 'images/72b97d36f92279ce64767c806bf25366.jpg', 2, 99, NULL, 'Vải', 'Trung Quốc', '2019-04-23 12:26:25', 7, 0, '2019-04-27 11:24:50'),
(1294888928, 'Chó ký ức - Gấu bông phim Thư Ký Kim Sao Thế ?', 280000, 'images/766062092736a2458e882f64bec79a9d.jpg', 2, 45, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1345968749, 'Thú bông MUMUSO thỏ con nằm ngộ nghĩnh', 350000, 'images/fcd73c6ace3278463024c4748cf3a20c.jpg', 2, 97, NULL, 'Vải', 'Trung Quốc', '2019-04-23 12:26:25', 9, 0, '2019-04-27 11:24:50'),
(1328379115, 'Búp Bê Thời Trang Sariel  - 8828-C', 167000, 'images/e12b40148a700c666a351d852666184a.jpg', 2, 10, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(285477791, 'Nhà búp bê có đèn PINK GIRL', 250000, 'images/e33fc1f556fedb40247a245d2b213adf.jpg', 2, 30, NULL, 'Gỗ', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1273730799, 'Búp bê bé gái ngộ nghĩnh', 180000, 'images/cf06af567d90bad9eaf4dae50a9a9a79.jpg', 2, 74, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1328379133, 'Búp Bê Thời Trang Sariel  - 7721-D', 251000, 'images/f7251f679971aab7a1b228481ad99997.jpg', 2, 10, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1090346225, 'Gấu Bắc Cực Size 90cm (To nhất trong hình,có ảnh thật)', 400000, 'images/4fed5d56b35235c36d1ba434affffdbe.jpg', 2, 10, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1242013389, 'Búp bê /Thú Nhồi Bông', 220000, 'images/0909851b68de9f2c1743d3f9bff40bbd.jpg', 2, 44, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(971114904, 'Nhà búp bê - Cake Diary', 400000, 'images/8924ede46918dc4d51339e7510107d01.jpg', 2, 8, NULL, 'Gỗ', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(988359148, 'Búp Bê Khớp Cô Dâu Xinh Đẹp ( Đỏ ) Tặng Kèm 3 Váy Ngắn', 98000, 'images/53108fb260351f7d0fb3cfeee440d29d.jpg', 2, 5, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1235741937, 'Búp Bê Nhồi Bông Cho Bé Dài 45cm', 200000, 'images/b5480199bec9b800f77a9ba528df6822.jpg', 2, 48, NULL, 'bông', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(151592755, 'Mô Hình Lắp Ráp Daban MG Gundam Astray Red Frame', 749000, 'images/6ff492d9f8b059a512ff4e1e58792675.jpg', 3, 15, '', 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 4, 0, '2019-04-27 11:24:50'),
(164255383, 'Đế trưng bày Gundam Action Base cỡ 1/144 - Dụng cụ(Đen)', 79000, 'images/6e2417102405f23971acb62b84fdafe9.jpg', 3, 2, '', 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1300903249, 'Mô Hình Lắp Ráp Gundam Bandai HGBD 009 Gundam 00 Diver Ace', 899000, 'images/be99a0744f9a38d82e257b11c96c04d4.jpg', 3, 6, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 13, 1, '2019-04-27 11:24:50'),
(1069752810, 'Bộ Lắp Ráp Khám Phá Vũ Trụ Ausini 25469 (129 Mảnh Ghép)', 114000, 'images/d6f2bc927a410a6e2e4a5367c53881fd.jpg', 3, 7, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(438456484, 'Mô Hình Lắp Ráp SD Wing Angel', 299000, 'images/435fd30139c6a42300f32fb0c542aace.jpg', 3, 2, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(82296300, 'Mô Hình Lắp Ráp Bandai SD EX-Standard Gundam Exia', 249000, 'images/0cf85776cd3a4f4c2b4195307363c0ab.jpg', 3, 3, 'BANDAI', 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(425223749, 'GMS-121 Gundam Metallic Marker Set', 449000, 'images/a05edc455c7d885cff36112c3bdbeb9b.jpg', 3, 2, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1069752864, 'Bộ Lắp Ráp Khám Phá Vũ Trụ Ausini 25467 (146 Mảnh Ghép)', 108000, 'images/29ea020a43c86ad3a398915e72d81f9b.jpg', 3, 9, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1141301370, 'Bộ Lắp Ráp Thành Phố Hiện Đại Ausini 25101 (32 Mảnh Ghép)', 16000, 'images/315c14b701fdbc6cd1f1b75d6b53b9cc.jpg', 3, 10, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(216775935, 'Mô hình lắp ráp BANDAI High Grade GUNDAM IRON BLOODED ORPHANS Gundam Bael', 499000, 'images/6cfe6c088b0fbc3f1ad4f021e0184ac4.jpg', 3, 1, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(199196538, 'Mô hình lắp ráp BANDAI HG GUNDAM IBO Gundam Barbatos Lupus Rex', 499000, 'images/20dfb8959f16f21a9631cdea9b6067fb.jpg', 3, 1, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1141311229, 'Bộ Lắp Ráp Thành Phố Hiện Đại Ausini 25102 (33 Mảnh Ghép)', 20000, 'images/2e92ae380f681fbc1369dba3d14c0faf.jpg', 3, 10, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1069752767, 'Bộ Lắp Ráp Khám Phá Vũ Trụ Ausini 25362 (69 Mảnh Ghép)', 50000, 'images/8f5be5c50bc25ec35316c7e8d3411e81.jpg', 3, 10, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(220035112, 'Mô hình lắp ráp Bandai SD EX-standard Banshee Norn (Destroy Mode)', 249000, 'images/31b41b84168788aa260cfdf70d9ca1e8.jpg', 3, 2, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(197840698, 'Mô hình lắp ráp High Grade Build Fighters Kamiki Burning Gundam', 599000, 'images/d59cf9970d9305218fa71bb355619d00.jpg', 3, 8, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 6, 0, '2019-04-27 11:24:50'),
(425407018, 'Mô Hình Lắp Ráp Bandai High Grade GUNDAM IBO Gundam Astaroth', 479000, 'images/5ae3a363f0d8d0c377055fcfe8c4c327.jpg', 3, 1, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 15, 0, '2019-04-27 11:24:50'),
(1069767935, 'Bộ Lắp Ráp Khám Phá Vũ Trụ Ausini 25472 (126 Mảnh Ghép)', 123000, 'images/221996b1412621d10f93311393ae594d.jpg', 3, 9, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1069768060, 'Bộ Lắp Ráp Đường Đua Rực Lửa Ausini 26404 (170 Mảnh Ghép)', 175000, 'images/69ff65f013480d3fd023ba0dd31bd885.jpg', 3, 10, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(197835860, 'Mô Hình Lắp Ráp High Grade GUNDAM THE ORIGIN Guntank Early Type', 619000, 'images/648072084eeef74b883882402f4fa19f.jpg', 3, 1, NULL, 'Nhựa', 'Japan', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1069752587, 'Bộ Lắp Ráp Thành Phố Hiện Đại Ausini 25204 (56 Mảnh Ghép)', 36000, 'images/4e5777157e23d0f5f36aaf7c1e461c88.jpg', 3, 10, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1069767953, 'Bộ Lắp Ráp Đường Đua Rực Lửa Ausini 26401 (159 Mảnh Ghép)', 138000, 'images/c24698bba39e02bb77de927315917f42.jpg', 3, 10, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(80826484, 'Mô Hình Lắp Ráp Bandai RG Gundam Astray Red Frame', 799000, 'images/de68c0bc805ce2f9c45aec3f33b4339c.jpg', 3, 3, 'Bandai', 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(164287542, 'Đế trưng bày Gundam Action Base cỡ 1/100 (Trắng) - Dụng cụ', 119000, 'images/9dd71763b3d3b33731d402095ff23e28.jpg', 3, 4, '', 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(164359444, 'Bút kẻ lằn chìm GM302 - Dụng cụ', 129000, 'images/bbd3738d7f0aa6ec91e2c3a15142dc09.jpg', 3, 1, '', 'Nhựa', 'Japan', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(82296543, 'Mô hình lắp ráp Bandai SD EX-standard Gundam Try Burning', 249000, 'images/d8e7c29746be500d9a2647c700a5b02d.jpg', 3, 5, 'BANDAI', 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1223554618, 'Mô Hình Lắp Ráp Gundam SD 391 Unicorn Gundam 02 Banshee Norn', 329000, 'images/8d365b8ed4cc3d6b12584e9be6b235ba.jpg', 3, 2, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1090460965, 'Mô Hình Lắp Ráp Bandai HG Gundam 00 Diver', 399000, 'images/d1748fd48b617969642344bd9249eb7e.jpg', 3, 8, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(82238519, 'Mô hình lắp ráp Bandai RG Destiny Gundam', 799000, 'images/c49343e6001e100c22c8f873e56fa26d.jpg', 3, 3, 'BANDAI', 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(81768635, 'Mô hình lắp ráp Bandai HG BF Amazing Exia', 599000, 'images/89784c41193f1ea2827e007da441b91e.jpg', 3, 5, 'BANDAI', 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1042479709, 'Mô Hình Lắp Ráp Bandai RG 25 Unicorn Gundam (Bande Dessinee Ver)', 1499000, 'images/ddf57fbc1ef5bea4fd608627587e5952.jpg', 3, 1, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(164366693, 'Bút kẻ lằn chìm GM303 - Dụng cụ', 129000, 'images/e8f3149e78511c9f1bc362275891c74e.jpg', 3, 8, '', 'Nhựa', 'Japan', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1237969272, 'Mô Hình Lắp Ráp Gundam Bandai HGBD 004 Momokapool', 749000, 'images/8cb5056d443228b9944788fe72cb3246.jpg', 3, 1, NULL, 'Nhựa', 'Nhật Bản', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1329188082, 'Hiệu ứng cánh cho Wing Gundam Zero Custom cỡ RG', 99000, 'images/27dc03d967270f0f823349850e09dbf2.jpg', 3, 1, NULL, 'giấy', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(164554093, 'BỘ CỜ CÁ NGỰA GIẢI TRÍ CHO BÉ', 10000, 'images/10491c3eeafcd735e9f635c06f49ffdb.jpg', 4, 10, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(33383964, 'Trò Chơi Thẻ bài Mèo Nổ Exploding Kittens BoardgameVN', 200000, 'images/4ff78ab7a937b1e4baa4997bed632625.jpg', 4, 68, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 4, 0, '2019-04-27 11:24:50'),
(685245977, 'Hộp đồ chơi bộ cờ Vua Quốc Tế bằng nhựa Liên Thành', 30000, 'images/485ba8806e93af09d8c08b42b8cac46b.jpg', 4, 976, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(66931520, 'Bộ Đồ Chơi Đâm Hải Tặc Cỡ Lớn Cho Bé', 70000, 'images/f06900d8ff1566481268dc7b151d185f.jpg', 4, 982, '', 'Nhựa', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(391097434, 'Card game Ma Sói Ultimate Deluxe (Việt Hoá) - Chính hãng BoardgameVN', 240000, 'images/52c6c00c2c598f79e4a89322028b56cd.jpg', 4, 259, 'BoardgameVN', 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(490425228, 'Bàn Cờ Tướng Loại 1', 55800, 'images/f7ee85034914de926854ea6575a1c8c1.jpg', 4, 952, NULL, 'Gỗ', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(79616449, 'Đồ chơi rút gỗ thông minh (48 thanh)', 25000, 'images/3a8d3eba5826a983636453203af40363.jpg', 4, 1876, 'Lego', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(685298705, 'Hộp đồ chơi bộ cờ Tỷ Phú bằng nhựa Vĩnh Phát', 20000, 'images/27a76ba77ea9d1ca887d1cd18859359e.jpg', 4, 65, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(24280731, 'Đồ chơi đâm hải tặc loại trung', 55000, 'images/d2a52d2f9c362b758d452d26b2f85a92.jpg', 4, 981, '', 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(502187217, 'Đồ chơi chó nổi giận (chó gặm xương) - Bộ to', 200000, 'images/16b806a566787cc488a6745e88eacfc7.jpg', 4, 46, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(874592305, 'Thẻ bài Uno Storm BoardgameVN - Bản Mở Rộng #2', 60000, 'images/86483f5780f6dac79341b956ee865328.jpg', 4, 295, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(986805881, 'Bọc bài UNO 5.7 x 8.7 cm - BoardgameVN', 20000, 'images/e82ccda512bf80ae5cbda4f822da89cf.jpg', 4, 121, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(350846691, 'Sleeves Bọc bài thường cho Mèo nổ - Exploding Kittens 100 cái', 20000, 'images/2935b5a8b64c549b8482644418945b31.jpg', 4, 205, 'Boardgamevn', 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(29958666, 'Bộ đồ chơi rút gỗ 54 thanh mini', 35000, 'images/96674dc2814f59f1098a0cb5d1676e06.jpg', 4, 9783, '', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1026149077, 'Thẻ bài Ma sói Characters Plus - Bản mở rộng của Ma Sói Characters BoardgameVN', 30000, 'images/59fce62932a5188932989019b4642465.jpg', 4, 186, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(980219422, 'Thẻ bài Ma Sói Artifact BoardgameVN - Cổ Vật Huyền Thoại (Bản mở rộng Ultimate)', 200000, 'images/254c896e7a01ff4271bd657cf8bc9b57.jpg', 4, 91, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(522605742, 'Bộ Cờ 3 Trong 1 - Ô Ăn Quan - Cờ Vây - Caro', 120000, 'images/890c6069f2174ec2147ab7c8a7c6082f.jpg', 4, 191, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(502540002, 'Combo Card game Nổ Tưng Bừng Combo Mèo nổ Exploding Kittens và 4 Bản mở rộng', 345000, 'images/043fad37185a227d66c3a6c8d76534a2.jpg', 4, 195, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(11281883, 'Khám răng cá sấu -dc1779', 45000, 'images/29b7ea5206423fc1b3007fb324b66e8d.jpg', 4, 295, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(188922453, 'Combo thẻ bài Uno và Ma sói Character Việt hóa BoardgameVN', 230000, 'images/57ddc3245a155c31e514fa0ed4c68652.jpg', 4, 91, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(341498550, 'Bộ đồ chơi câu cá 4 hồ', 90000, 'images/a438aa54ac2fd4a2aa0a21a360950196.jpg', 4, 43, NULL, 'Nhựa', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(188859777, 'Bọc bài thường cho Ma sói Ultimate Deluxe 100 cái - BoardgameVN', 20000, 'images/b3a925430ab43e6832f47924a0150dfc.jpg', 4, 461, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(261699436, 'Bộ Cờ Tướng', 50000, 'images/f185405bd32c0b23a20a9eac27e4346b.jpg', 4, 84, 'oem', 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(245592968, 'Bộ cờ vua nam châm cao cấp', 170000, 'images/d5764fd3dbeffa17d9e4eb6a0ece27c7.jpg', 4, 2998, '', 'Nhựa', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(215596522, 'ROBOT KHÔNG GIAN XOAY 360 ĐỘ THEO NHẠC CỰC HOT', 125000, 'images/1a400ac0c3f49e460177d420174500e7.jpg', 4, 1955, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(10649627, 'Khám răng cá sấu cực thú vị', 75000, 'images/3a6720a6b324fa934d086444330fa31b.jpg', 4, 21, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(188886858, 'Uno Battle - Bản Mở Rộng #1', 60000, 'images/68b4811ddf5982ffed65fd113cfaf3c6.jpg', 4, 287, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1029032065, 'Bộ Cờ cá Ngựa', 55000, 'images/293d5aae0676168e856bced6fe7d3c21.jpg', 4, 11999, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(535748724, 'BỘ CỜ VUA NAM CHÂM CAO CẤP', 170000, 'images/d5764fd3dbeffa17d9e4eb6a0ece27c7.jpg', 4, 216, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(188437932, 'Thẻ bài Boardgame Bang Dodge City - Bản mở rộng Việt hoá BoardgameVN', 180000, 'images/6e44494921faa676115a99be09488a06.jpg', 4, 98, NULL, 'Giấy', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(94160717, 'Vỉ đồ chơi máy nuôi thú ảo - ĐỒ CHƠI CHỢ LỚN', 30000, 'images/433afe78a04a2c9efff8eeed7a73cc0b.jpg', 4, 5, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(966006928, 'Set Sum Vầy: Combo Đâm hải tặc + Khám răng cá sấu', 300000, 'images/f9dd3608bafd466096ad9878fc317cdd.jpg', 4, 4, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1058553019, 'Bộ cờ tướng xếp hình 3D bằng gỗ', 212000, 'images/092e3387412954740945fa0505ca0866.jpg', 4, 300, 'OEM', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1040191790, 'Bộ cờ vua', 190000, 'images/053fb8f3b3413c32312a12e3479f4339.jpg', 4, 5, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(944944594, 'BỘ CỜ VUA NAM CHÂM CAO CẤP (HÀNG LOẠI 1)', 172000, 'images/78ca60c76d3b41c5d7031ab9c55247ce.jpg', 4, 985, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1541944899, 'Combo Bảng Nam Châm Bé Làm Quen Số-Hình Học Và Vỉ Chữ Thường Nam Châm Antona', 229000, 'images/1f2cb62d5d5442d989917d23157677ec.jpg', 5, 449, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1542082504, 'Đồ Chơi Kệ Chữ A Mặt Trời Của Bé Antona Tặng Dụng Cụ Uống Thuốc Cho Bé', 259000, 'images/93208f60473126d4d0751dc36fc9aa75.jpg', 5, 490, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1579719705, '(Hàng Việt Nam) Đồ Chơi Ô Tô Thả Hình Khối Antona Bằng Nhựa An Toàn Cho Bé', 125000, 'images/56d2c7144f8a82c99e5360feb0002fd9.jpg', 5, 396, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1560424090, 'Đồ Chơi Nấu Ăn Bữa Cơm Gia Đình Đỏ Antona Cho Bé', 189000, 'images/96cdead6fdcef59f218da7c8cac4155c.jpg', 5, 491, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1541982558, 'Combo Bảng Nam Châm Bé Làm Quen Chữ Thường Và Vỉ Số Nam Châm Antona', 199000, 'images/4b87a2183ff8575e8277706d1dceec9b.jpg', 5, 481, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1483201901, 'Combo 3 Vỉ Học Chữ Cái Thường Chữ In Hoa Và Chữ Số Gắn Nam Châm Antona', 199000, 'images/4ced0c13407ac9ce6cc922b75691736c.jpg', 5, 434, NULL, 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1047473900, 'Bảng điện tử thông minh - Lovelykid', 99000, 'images/b9e8485b11194e5b1e05e0085dad7ddc.jpg', 5, 206, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022115232, 'Bộ 26 Miếng Thảm Cao Su Phước Thành Hình Chữ Cái (30 x 30 cm)', 200000, 'images/a3ab0d0949e294fa57b740d40262adcb.jpg', 5, 12, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(856198630, 'Xúc xắc trẻ em Antona hộp 5 chiếc AX07 C017', 135000, 'images/6060df634aeaf679396373174ec60464.jpg', 5, 27, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(888579478, 'Bảng chữ số đa năng Antona - Mẫu 2: Chữ thường (mới)', 169000, 'images/e636bf1cee0f289fda6e4ce232fc27da.jpg', 5, 21, 'Antona', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022268138, 'Đàn Xylophone 7 thanh Forkids FKS-093', 130000, 'images/60b7a61f5b50b321412f6b1b8b7c1408.jpg', 5, 26, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022115215, 'Bộ 10 Miếng Thảm Cao Su Phước Thành Hình Chữ Số (30 x 30 cm)', 90000, 'images/2e76d0251870138b52d44f0db54f3b6c.jpg', 5, 16, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1227964483, 'Bộ chơi cát nhiều chi tiết cho bé SAND', 139000, 'images/c0a644f751f7e6aa350ee6b89e5fdedd.jpg', 5, 37, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1147537282, 'Bộ chữ số nam châm Antona AN04', 45000, 'images/c8511ca70bddeed670b74ce6aa56e8ef.jpg', 5, 10, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1147537225, 'Hình vuông diệu kỳ - 69 chi tiết Antona AV36', 99000, 'images/64f1a7609d57fc5c7696de5315f55961.jpg', 5, 31, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1047473896, 'Bảng điện tử thông minh - 11 trong 1 Lovelykid LB02', 180000, 'images/b9e8485b11194e5b1e05e0085dad7ddc.jpg', 5, 7, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1098259044, 'Cát nặn Kid Art', 109000, 'images/68589ff5b5713af8b41bf942271ac448.jpg', 5, 27, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022115207, 'Bộ 10 Miếng Thảm Cao Su Phước Thành Hình Vũ Trụ (30 x 30 cm)', 90000, 'images/1a6eac366230405b70def256e3095507.jpg', 5, 25, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022115217, 'Bộ 10 Miếng Thảm Cao Su Phước Thành Hình Phương Tiện Giao Thông (30 x 30 cm)', 90000, 'images/3cf891b4d0e545f92d9cc262794316c0.jpg', 5, 32, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1147537227, 'Bộ chữ nam châm ABC Antona AN13', 45000, 'images/4ca55394b02ad57d5feaa4b7bce6d573.jpg', 5, 12, NULL, '', '', '2019-04-23 12:26:25', 3, 0, '2019-04-27 11:24:50'),
(1022268157, 'Hộp thả hình Forkids FKS-094', 169000, 'images/dd2f85d0474db7643f011ffc2f867292.jpg', 5, 44, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1476601803, 'Đồ Chơi Xếp Vòng Mầm Non Sato Giúp Bé Nhận Thức Kích Thước Màu Sắc', 109000, 'images/3a59edcc804dc68bca59d3777f8384d8.jpg', 5, 23, 'Sato', 'Nhựa', 'Việt Nam', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022231664, 'Đàn Xylophone 5 thanh Forkids FKS-092', 125000, 'images/472445873473f7e3c54d3b989575d1d1.jpg', 5, 23, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595211, 'Bộ 3 Sách Vải Kèm Gặm Nướu Pipovietnam (Động Vật Nuôi - Sinh Vật Biển - Côn Trùng)', 249000, 'images/09c4b5989cb73ac04cdeb0ec83256e52.jpg', 5, 19, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1086442068, 'Thú bông KonigKids TBKK-5123', 138000, 'images/515a5dfe9569a921ed08ba39658ab0d1.jpg', 5, 17, NULL, 'Vải', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022231641, 'Cân thăng bằng Forkids FKS-059', 89000, 'images/3d109e6654062bbf8f91c46b7889dd56.jpg', 5, 26, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1086442094, 'Set 3 món trống nhỏ kèm xúc xắc Winfun - 2026', 179000, 'images/51a46615b3e7fe74e567db7a9e93bd31.jpg', 5, 19, NULL, 'Nhựa', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595250, 'Bộ 3 Sách Vải Kèm Gặm Nướu Pipovietnam (Thời Tiết - Chữ Cái Tiếng Việt - Số Đếm)', 249000, 'images/92f436aafe7bbc876a163920abbf1e22.jpg', 5, 6, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022231638, 'Hề tháp 9 vòng Forkids FKS-064', 139000, 'images/3c95af78006d6fe5e2632d6b2aa26397.jpg', 5, 42, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1147537236, 'Bảng nam châm Antona - Bé làm quen chữ hoa – Tiếng Việt, tiếng Anh AN07', 109000, 'images/d180d70e87f98af71c50cfcd66974f9a.jpg', 5, 4, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1098259014, 'Bộ đồ chơi xếp hình khối trí tuệ Pamama (P0111)', 109000, 'images/9fb59c48cadf5645239fbda1d05440d7.jpg', 5, 6, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1086442097, 'Xúc xắc chíp chíp xinh xắn Toys House 0090', 49000, 'images/194f81135d918287cd844f6fcb691f62.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595267, 'Bộ 4 Sách Vải Pipovietnam (Sinh Vật Biển - Động Vật Ăn Cỏ - Ăn Thịt - Động Vật Nuôi)', 249000, 'images/e21ac4b6c83ff3530e921eb790718345.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595251, 'Bộ 4 Sách Vải Pipovietnam (Hoa Quả - Chữ Cái Tiếng Việt - Số Đếm - Hình Khối)', 249000, 'images/55d6c6c3f58ff21cf27fb5307c52c131.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1147537226, 'Bảng nam châm Antona  5 trong 1 cải tiến - Đỏ AN14', 189000, 'images/70518cfa6be89fd8b2efd7336942e7cc.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1047473885, 'Ngậm nướu Sâu nhỏ/ Ô tô Antona AS07 - AS08', 89000, 'images/ba246e83b0e207780cdfcc8b7e48e6f7.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(962140929, 'Đồ Chơi nhà tắm Toys house - 6 món (TL811-2)', 109000, 'images/40aa8406c0202338763ef37c87a85392.jpg', 5, 0, NULL, '', 'Trung Quốc', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1098259018, 'Bộ tô tượng Cá heo/Voi Star Kids K101', 42000, 'images/55173d5098c8a1148a9073e903fa2fde.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022115226, 'Bộ 40 Miếng Thảm Cao Su Phước Thành Hình Chữ Cái Và Số (15 x 15 cm)', 90000, 'images/c26c8f544df483ef767fc8b4ea73276e.jpg', 5, 0, 'Phước Thành', '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595218, 'Sách Vải Pipovietnam Rau Củ Quả', 79000, 'images/d652ce6eec35f908fb3a0aa1e6111494.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595209, 'Sách Vải Pipovietnam Chữ Cái Tiếng Việt', 79000, 'images/8541fc698e76eb49bb4604c51a9f039c.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595226, 'Sách Vải Pipovietnam Hình Phương Tiện Giao Thông', 79000, 'images/9fae7fe39afa075be9872b9b74042a72.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1040282197, ' Assembly tank truck game ( Xe bồn lắp rắp) - Colligo 30231 ', 109000, 'images/05f36d2704f8cc4cda025ab952b01d92.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595243, 'Sách Vải Trẻ Em Pipovietnam Hoa Quả', 79000, 'images/a7cea461a51f43b250f82a0e75f2d725.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595237, 'Bộ 3 Sách Vải Pipovietnam (Hoa Quả - Chữ Cái Tiếng Việt - Số Đếm)', 199000, 'images/b9645d0beaaa05271fa6b150a5d0c0fe.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1039595257, 'Bộ 3 Sách Vải Kèm Gặm Nướu Pipovietnam Động Vật', 249000, 'images/93a29aa6d5418ed153505ee54d85c58c.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1098259019, 'Bộ tô gỗ Thỏ/Voi/Gà/Hươu Star Kids', 59000, 'images/789c08eb29f80734346ab23ee92fbda6.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50'),
(1022268146, 'Xe cũi thả hình Vietoys VT3P-0109', 139000, 'images/a673dad1f5adb0942a826f7594def4d0.jpg', 5, 0, NULL, '', '', '2019-04-23 12:26:25', 2, 0, '2019-04-27 11:24:50');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
