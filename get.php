<?php include "header.php"; ?>
<div class="wrapper">
    <input class="form-control" id="link">
    <button type="button" id="submit">Submit</button>
</div>

<div class="page-content">
    <textarea id="output" style="width: 100%" rows="100"></textarea>
</div>

<script>
    $(document).on("click", "#submit", () => {
        let id = $("#link").val();
        $.get("https://shopee.vn/api/v2/search_items/?by=pop&limit=50&match_id=" + id + "&newest=0&order=desc&page_type=collection", result => {
            result.items.forEach(r => {
                let itemid = r.itemid;
                let shopid = r.shopid;
                $.get("https://shopee.vn/api/v2/item/get?itemid=" + itemid + "&shopid=" + shopid, response => {
                    response = response.item;
                    let item = {
                        masanpham: response.itemid,
                        tensanpham: response.name,
                        hinhanh: 'https://cf.shopee.vn/file/' + response.image,
                        soluong: response.stock,
                        gia: response.price_before_discount / 100000,
                        thuonghieu: response.brand,
                        xuatxu: '',
                        chatlieu: ''
                    };
                    if (response.attributes)
                        response.attributes.forEach(x => {
                            if (x.id == 19382)
                                item.chatlieu = x.value;
                            if (x.id == 19380)
                                item.xuatxu = x.value;
                        });
                    sql = "INSERT INTO sanpham(masanpham, tensanpham, madanhmuc, gia, hinhanh, soluong, thuonghieu, chatlieu, xuatxu) VALUES ('" + item.masanpham + "', '" + item.tensanpham + "', 5, '" + item.gia + "', '" + item.hinhanh + "', '" + item.soluong + "', '" + item.thuonghieu + "', '" + item.chatlieu + "', '" + item.xuatxu + "');";
                    $("#output").val($("#output").val() + "\n" + sql);
                });
            });
        });
    });
</script>
<?php include "footer.php" ?>
