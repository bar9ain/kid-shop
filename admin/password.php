<?php
include "header.php";

$tendangnhap = $_SESSION["admin"];

if (isset($_POST["update_password"])) {
    $matkhaucu = $_POST["matkhaucu"];
    $matkhau = $_POST["matkhau"];
    $matkhau2 = $_POST["matkhau2"];
    if ($matkhau !== $matkhau2) {
        $error = "Mật khẩu và mật khẩu xác nhận không giống nhau";
    } else {
        $sql = "SELECT matkhau
                FROM admin
                WHERE tendangnhap = '$tendangnhap'";
        $query = $db->query($sql);
        $result = $query->fetch_assoc();
        if ($matkhaucu !== $result["matkhau"]) {
            $error = "Mật khẩu cũ không chính xác";
        } else {
            $sql = "UPDATE admin
                    SET matkhau = '$matkhau'
                    WHERE tendangnhap = '$tendangnhap'";
            if ($db->query($sql)) {
                $alert = "Mật khẩu mới đã được cập nhật!";
                $redirect = "password.php";
            }
        }
    }
}
?>

<form method="post">
    <div class="authen-modal">
        <div class="authen-header">
            <a class="authen-header-tab active">Đổi mật khẩu</a>
        </div>
        <div class="authen-body">
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="password"
                       name="matkhaucu"
                       placeholder="Mật khẩu hiện tại"
                       autofocus
                       required>
            </div>
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="password"
                       name="matkhau"
                       placeholder="Mật khẩu"
                       required>
            </div>
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="password"
                       name="matkhau2"
                       placeholder="Xác nhận mật khẩu"
                       required>
            </div>
            <?php if (isset($error)) { ?>
                <div class="authen-error"><?= $error ?></div>
            <?php } ?>
        </div>
        <div class="authen-footer">
            <button type="submit" name="update_password" class="btn btn-submit">
                Cập nhật
            </button>
        </div>
    </div>
</form>
<?php include "footer.php" ?>
