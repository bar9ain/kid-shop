<?php
include "header.php";
if (!isset($_SESSION["makhachhang"])) {
    $alert = "Bạn phải đăng nhập để thực hiện mua hàng!";
    $hoten = $diachi = $sdt = $email = "";
} else {
    $hasError = false;
    if (isset($_POST["checkout_confirm"])) {
        try {
            $madonhang = time();
            $makhachhang = $_POST["makhachhang"];
            $tongtien = $_POST["tongtien"];

            // Tạo đơn hàng
            $db->begin_transaction();
            $db->autocommit(false);
            $sql = "INSERT INTO donhang (
                    madonhang,
                    makhachhang,
                    tongtien
                ) VALUES (
                    $madonhang,
                    $makhachhang,
                    $tongtien
                )";
            $db->query($sql);

            $giohang = $_SESSION["giohang"];
            foreach ($giohang as $item) {
                // Kiểm tra số lượng tồn từng sản phẩm
                $sql = "SELECT soluong FROM sanpham WHERE masanpham = $item->masanpham";
                $query = $db->query($sql);
                $row = $query->fetch_assoc();
                if ($row["soluong"] < $item->soluong) {
                    $alert = "Số lượng sản phẩm \"$item->tensanpham\" còn lại không đủ!";
                    $hasError = true;
                    break;
                }

                // Insert chi tiết đơn hàng
                $sql = "INSERT INTO chitietdonhang (
                            madonhang,
                            masanpham,
                            tensanpham,
                            dongia,
                            soluong
                        ) VALUES (
                            '$madonhang',
                            '$item->masanpham',
                            '$item->tensanpham',
                            '$item->gia',
                            '$item->soluong'
                        )";
                $db->query($sql);

                // Cập nhật số lượng tồn
                $sql = "UPDATE sanpham
                    SET soluong = soluong - $item->soluong,
                        luotmua = luotmua + $item->soluong
                    WHERE masanpham = $item->masanpham";
                $db->query($sql);
            }

            // Nếu có lỗi xảy ra thì rollback dữ liệu, nếu thành công thì xóa giỏ hàng và thông báo.
            if ($hasError) {
                $db->rollback();
            } else {
                $db->commit();
                unset($_SESSION["giohang"]);
                include "giohang_success.php";
                return;
            }
        } catch (Exception $e) {
            $hasError = true;
            $alert = $e->getMessage();
            $db->rollback();
            include "footer.php";
            return;
        }
    }

    $makhachhang = $_SESSION["makhachhang"];
    $sql = "SELECT
                makhachhang,
                hoten,
                diachi,
                sdt,
                email
            FROM khachhang
            WHERE makhachhang='$makhachhang'";
    $query = $db->query($sql);
    $row = $query->fetch_assoc();
    if (isset($row)) {
        $hoten = $row["hoten"];
        $diachi = $row["diachi"];
        $sdt = $row["sdt"];
        $email = $row["email"];
    }
}
?>

    <div class="flex-wrap">
        <div class="card half transparent flex-column">
            <div class="tieudedanhmuc">Giỏ hàng</div>
            <div class="cart-list">
                <?php
                $giohang = $_SESSION["giohang"];
                $index = 0;
                $summary = 0;
                foreach ($giohang as $item) {
                    ?>
                    <div class="cart-row">
                        <div class="cart-product">
                            <img alt="" src="<?= $item->hinhanh ?>">
                            <?= $item->tensanpham ?>
                        </div>
                        <div class="cart-quantity"><?= $item->soluong ?></div>
                        <div class="cart-cost">₫<?= number_format($item->gia * $item->soluong) ?></div>
                    </div>
                    <?php
                    $index++;
                    $summary += $item->gia * $item->soluong;
                }
                ?>
            </div>
        </div>

        <div class="card half transparent flex-column">
            <div class="tieudedanhmuc">Thông tin khách hàng</div>
            <div class="chitietkhachhang">
                <div class="chitiet">
                    <div class="title">Họ tên:</div>
                    <div class="content"><?= $hoten ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Địa chỉ:</div>
                    <div class="content"><?= $diachi ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Số điện thoại:</div>
                    <div class="content"><?= $sdt ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Email:</div>
                    <div class="content"><?= $email ?></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="cart-footer">
                <div class="cart-cost">
                    <div class="title">Tổng tiền:</div>
                    <div class="content">₫<?= number_format($summary, 0, 3, '.') ?></div>
                </div>
                <?php if (isset($makhachhang)) { ?>
                    <form method="post">
                        <input type="hidden" name="makhachhang" value="<?= $makhachhang ?>">
                        <input type="hidden" name="tongtien" value="<?= $summary ?>">
                        <button class="button" name="checkout_confirm">Xác nhận</button>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>

<?php include "footer.php" ?>