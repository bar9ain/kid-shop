<?php include "header.php";
$sql = "SELECT makhachhang, hoten, diachi, email, sdt FROM khachhang";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<div class="card flex-column">
    <div class="card-header">
        <div class="card-header-title">Danh sách khách hàng</div>
    </div>
    <div class="card-body">
        <table class="admin-table">
            <tr class="admin-table-row">
                <th class="admin-table-header">Mã khách hàng</th>
                <th class="admin-table-header">Họ tên</th>
                <th class="admin-table-header">Địa chỉ</th>
                <th class="admin-table-header">Email</th>
                <th class="admin-table-header">SĐT</th>
            </tr>
            <?php foreach ($list as $item) { ?>
                <tr class="admin-table-row">
                    <td class="admin-table-data"><?= $item["makhachhang"] ?></td>
                    <td class="admin-table-data"><?= $item["hoten"] ?></td>
                    <td class="admin-table-data"><?= $item["diachi"] ?></td>
                    <td class="admin-table-data"><?= $item["email"] ?></td>
                    <td class="admin-table-data"><?= $item["sdt"] ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<?php include "footer.php" ?>
