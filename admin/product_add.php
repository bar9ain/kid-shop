<?php include "header.php";

if (isset($_POST["tensanpham"])) {
    $tensanpham = $_POST["tensanpham"];
    $madanhmuc = $_POST["madanhmuc"];
    $gia = $_POST["gia"];
    $soluong = $_POST["soluong"];
    $thuonghieu = $_POST["thuonghieu"];
    $chatlieu = $_POST["chatlieu"];
    $xuatxu = $_POST["xuatxu"];

    if (isset($_FILES["hinhanh"]) && $_FILES["hinhanh"]["size"] > 0) {
        $time = time();
        $type = explode("/", $_FILES["hinhanh"]["type"])[1];
        $hinhanh = "images/$time.$type";
        move_uploaded_file($_FILES["hinhanh"]["tmp_name"], "../" . $hinhanh);
    }
    $sql = "INSERT INTO sanpham(tensanpham, madanhmuc, gia, hinhanh, soluong, thuonghieu, chatlieu, xuatxu)
            VALUES ('$tensanpham', '$madanhmuc', '$gia', '$hinhanh', '$soluong', '$thuonghieu', '$chatlieu', '$xuatxu')";
    if ($db->query($sql)) {
        header("Location: product_list.php");
    }
}

$sql = "select * from danhmucsanpham";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>

    <form method="post" enctype="multipart/form-data">
        <div class="edit-page">
            <div class="edit-header">
                <div class="edit-header-title">Thêm sản phẩm</div>
                <div class="edit-header-subtitle">Add product</div>
            </div>
            <div class="edit-body">
                <div class="edit-body-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên sản phẩm</div>
                        <div class="input-with-label-content">
                            <input type="text" name="tensanpham" class="input-with-status-input" required autofocus>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Danh mục sản phẩm</div>
                        <div class="input-with-label-content">
                            <select class="input-with-status-input" name="madanhmuc">
                                <?php foreach ($list as $item) { ?>
                                    <option value="<?= $item["madanhmuc"] ?>"><?= $item["tendanhmuc"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Giá</div>
                        <div class="input-with-label-content">
                            <input type="number" name="gia" class="input-with-status-input" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Số lượng</div>
                        <div class="input-with-label-content">
                            <input type="number" name="soluong" class="input-with-status-input" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Thương hiệu</div>
                        <div class="input-with-label-content">
                            <input type="text" name="thuonghieu" class="input-with-status-input">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Chất liệu</div>
                        <div class="input-with-label-content">
                            <input type="text" name="chatlieu" class="input-with-status-input">
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Xuất xứ</div>
                        <div class="input-with-label-content">
                            <input type="text" name="xuatxu" class="input-with-status-input">
                        </div>
                    </div>
                    <div class="edit-submit">
                        <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                        <a href="product_list.php" class="btn-cancel">Trở lại</a>
                    </div>
                </div>
                <div class="edit-body-right">
                    <div class="edit-thumbnail">
                        <div class="edit-thumbnail-image">
                            <img src="../images/empty.png">
                        </div>
                        <input type="file" accept="image/*" name="hinhanh" required>
                    </div>
                </div>
            </div>
        </div>
    </form>

<?php include "footer.php"; ?>