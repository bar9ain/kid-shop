<?php include "header.php";

function randomString($n)
{
    $result = "";
    $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $len = strlen($domain);
    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, $len - 1);
        $result = $result . $domain[$index];
    }
    return $result;
}

if (isset($_POST["forgot-password"])) {
    $email = $_POST["email"];
    $sdt = $_POST["sdt"];
    $sql = "SELECT makhachhang
            FROM khachhang
            WHERE email='$email'
            AND sdt='$sdt'";
    $check = $db->query($sql);
    if ($check->num_rows > 0) {
        $row = $check->fetch_assoc();
        $makhachhang = $row["makhachhang"];
        $matkhau = randomString(6);
        $sql = "UPDATE khachhang SET password = '$matkhau' WHERE makhachhang = '$makhachhang'";
        if ($db->query($sql)) {
            $alert = "Mật khẩu của bạn đã được đặt lại thành $matkhau";
            $redirect = "dangnhap.php";
        } else {
            $alert = $db->error;
        }
    } else {
        $authen_error = true;
    }
}

?>
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Quên mật khẩu</a>
                <a class="authen-header-tab" href="dangnhap.php">Đăng nhập</a>
            </div>
            <div class="authen-body">
                <p>Vui lòng nhập chính xác email và số điện thoại để đặt lại mật khẩu!</p>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="email"
                           name="email"
                           placeholder="Email"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="sdt"
                           placeholder="Số điện thoại"
                           required>
                </div>
                <?php if (isset($authen_error)) { ?>
                    <div class="authen-error">Thông tin bạn nhập vào không đúng</div>
                <?php } ?>
            </div>
            <div class="authen-footer">
                <a href="dangnhap.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="forgot-password" class="btn btn-submit">
                    Đặt lại mật khẩu
                </button>
            </div>
        </div>
    </form>
<?php include "footer.php" ?>