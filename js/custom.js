﻿$("#btn-minus").click(function () {
    let value = Number($("[name=soluong]").val());
    if (value === 1) return;
    $("[name=soluong]").val(value - 1);
});

$("#btn-plus").click(function () {
    let value = Number($("[name=soluong]").val());
    if (value === Number($("#count").text())) return;
    $("[name=soluong]").val(value + 1);
});