<?php
session_start();

if (isset($_POST["cart_delete_all"])) {
    unset($_SESSION["giohang"]);
}

include "header.php";

if (isset($_POST["cart_submit"])) {
    if (isset($_SESSION["makhachhang"])) {
        header("location: thanhtoan.php");
    } else {
        $alert = "Bạn phải đăng nhập để thực hiện mua hàng!";
    }
}

if (isset($_GET["delete"]) && isset($_SESSION["giohang"])) {
    $giohang = $_SESSION["giohang"];
    $index = $_GET["delete"];
    unset($giohang[$index]);
    $_SESSION["giohang"] = array_values($giohang);
    header("location: giohang.php");
}

if (!isset($_SESSION["giohang"]) || $_SESSION["giohang"] === null || count($_SESSION["giohang"]) === 0) {
    include "giohang_empty.php";
    return;
}
?>

<form method="post">
    <div class="card">
        <div class="cart-header">
            <div class="cart-product">Sản phẩm</div>
            <div class="cart-price">Đơn giá</div>
            <div class="cart-quantity">Số lượng</div>
            <div class="cart-cost">Số tiền</div>
            <div class="cart-action">Thao tác</div>
        </div>
    </div>

    <div class="card">
        <div class="cart-list">
            <?php
            $giohang = $_SESSION["giohang"];
            $index = 0;
            $summary = 0;
            foreach ($giohang as $item) {
                ?>
                <div class="cart-row">
                    <div class="cart-product">
                        <img alt="" src="<?= $item->hinhanh ?>">
                        <?= $item->tensanpham ?>
                    </div>
                    <div class="cart-price">₫<?= number_format($item->gia) ?></div>
                    <div class="cart-quantity"><?= $item->soluong ?></div>
                    <div class="cart-cost">₫<?= number_format($item->gia * $item->soluong) ?></div>
                    <div class="cart-action"><a href="giohang.php?delete=<?= $index ?>">Xóa</a></div>
                </div>
                <?php
                $index++;
                $summary += $item->gia * $item->soluong;
            }
            ?>
        </div>
    </div>

    <div class="card">
        <div class="cart-footer">
            <button class="button invert" name="cart_delete_all">Xóa giỏ hàng</button>
            <div class="cart-cost">
                <div class="title">Tổng tiền:</div>
                <div class="content">₫<?= number_format($summary, 0, 3, '.') ?></div>
            </div>
            <button class="button" name="cart_submit">Thanh toán</button>
        </div>
    </div>
</form>

<?php include "footer.php" ?>