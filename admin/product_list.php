<?php include "header.php";

if (isset($_POST["delete"])) {
    $masanpham = $_POST["delete"];
    $sql = "delete from sanpham where masanpham='$masanpham'";
    if ($db->query($sql)) {
        header("Location: product_list.php");
    }
}

$sql = "SELECT * FROM sanpham
        INNER JOIN danhmucsanpham
        ON sanpham.madanhmuc = danhmucsanpham.madanhmuc
        ORDER BY sanpham.ngaythem DESC";
$query = mysqli_query($link, $sql);
$list = array();
while ($row = mysqli_fetch_array($query)) {
    $list[] = $row;
}
?>

    <div class="card flex-column">
        <div class="card-header">
            <div class="card-header-title">Quản lý danh mục</div>
            <div class="card-header-button">
                <a href="product_add.php">Thêm mới</a>
            </div>
        </div>
        <div class="card-body">
            <table class="admin-table">
                <tr class="admin-table-row">
                    <td class="admin-table-header">Hình ảnh</td>
                    <td class="admin-table-header">Mã sản phẩm</td>
                    <td class="admin-table-header">Tên sản phẩm</td>
                    <td class="admin-table-header">Danh mục</td>
                    <td class="admin-table-header">Giá</td>
                    <td class="admin-table-header">Chức năng</td>
                </tr>
                <?php foreach ($list as $item) { ?>
                    <tr class="admin-table-row">
                        <td class="admin-table-data">
                            <img width="150" src="../<?= $item["hinhanh"] ?>" class="product-list-image"></td>
                        <td class="admin-table-data"><?= $item["masanpham"] ?></td>
                        <td class="admin-table-data"><?= $item["tensanpham"] ?></td>
                        <td class="admin-table-data"><?= $item["tendanhmuc"] ?></td>
                        <td class="admin-table-data"><?= number_format($item["gia"]) ?> VNĐ</td>
                        <td class="admin-table-data">
                            <form method="post">
                                <a href="product_edit.php?id=<?= $item["masanpham"] ?>" class="btn btn-default">Sửa</a>
                                <a href="sale_add.php?id=<?= $item["masanpham"] ?>" class="btn btn-default">Tạo khuyến mãi</a>
                                <button class="btn btn-danger" name="delete" value="<?= $item["masanpham"] ?>">Xóa
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>

<?php include "footer.php"; ?>