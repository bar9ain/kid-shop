<?php include "header.php";

// Danh sách sản phẩm mới
$sql = "SELECT
            sanpham.masanpham,
            sanpham.tensanpham,
            sanpham.hinhanh,
            sanpham.gia,
            khuyenmai.giatri,
            sanpham.gia * (100 - khuyenmai.giatri) / 100 as giakhuyenmai
        FROM sanpham
        LEFT JOIN khuyenmai ON khuyenmai.masanpham = sanpham.masanpham
                           AND khuyenmai.thoigianbatdau <= now() <= khuyenmai.thoigianketthuc
        ORDER BY thoigian DESC
        LIMIT 0, 12";
$query = mysqli_query($link, $sql);
$sanpham = array();
while ($row = mysqli_fetch_array($query))
    $sanpham[] = $row;

// Danh mục sản phẩm
$sql = "SELECT * FROM danhmucsanpham WHERE hienthitrangchu = 1";
$query = mysqli_query($link, $sql);
$danhmuc = array();
while ($row = mysqli_fetch_array($query))
    $danhmuc[] = $row;

?>

    <div class="banner"></div>

    <div class="tieudedanhmuc">Danh mục</div>
    <div class="card nowrap">
        <?php foreach ($danhmuc as $row) { ?>
            <a href="sanpham.php?dm=<?= $row["madanhmuc"] ?>">
                <div class="danhmuc">
                    <div class="hinhanh" style="background-image:url('<?= $row["hinhanhdanhmuc"] ?>')"></div>
                    <div class="tendanhmuc"><?= $row["tendanhmuc"] ?></div>
                </div>
            </a>
        <?php } ?>
    </div>

    <div class="card transparent">
        <div class="tieudedanhmuc">Đồ chơi mới</div>
        <?php foreach ($sanpham as $row) { ?>
            <div class="sanpham">
                <a href="chitiet.php?id=<?= $row["masanpham"] ?>">
                    <?php if ($row["giatri"]) { ?>
                        <span class="khuyenmai">-<?= $row["giatri"] ?>%</span>
                    <?php } ?>
                    <div class="hinhanhsanpham" style="background-image:url('<?= $row["hinhanh"] ?>')"></div>
                    <div class="tensanpham"><?= $row["tensanpham"] ?></div>
                    <div class="giasanpham">
                        <img src="images/coin.png" width="16">
                        <?php if ($row["giatri"]) { ?>
                            <b>₫<?= number_format($row["giakhuyenmai"]) ?></b>
                            <s>₫<?= number_format($row["gia"], 0, 3, '.') ?></s>
                        <?php } else { ?>
                            ₫<?= number_format($row["gia"], 0, 3, '.') ?>
                        <?php } ?>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

<?php include "footer.php" ?>