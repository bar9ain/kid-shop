<?php include "header.php";


// Điều kiện tìm kiếm
$madanhmuc = isset($_GET["dm"]) ? $_GET["dm"] : null;
$dm_text = $madanhmuc ? "where madanhmuc = $madanhmuc" : null;
$search = isset($_GET["search"]) ? $_GET["search"] : null;
$search_text = $search ? "where tensanpham like '%$search%'" : null;
$sort = isset($_GET["sort"]) ? $_GET["sort"] : "new";
$page = isset($_GET["page"]) ? $_GET["page"] : 1;
$sorted = "luotxem desc";
$join = "left";
$limit = "limit " . (($page - 1) * 15) . ", 15";
switch ($sort) {
    case "sold":
        $sorted = "luotmua desc";
        break;
    case "sale":
        $sorted = "giatri desc";
        $join = "inner";
        break;
    case "low":
        $sorted = "gia";
        break;
    case "high":
        $sorted = "gia desc";
        break;
}
// Get sản phẩm theo điều kiện
$sql = "SELECT
                sanpham.masanpham,
                sanpham.tensanpham,
                sanpham.hinhanh,
                sanpham.gia,
                khuyenmai.giatri,
                sanpham.gia * (100 - khuyenmai.giatri) / 100 as giakhuyenmai
            FROM sanpham
            $join JOIN khuyenmai ON khuyenmai.masanpham = sanpham.masanpham
                               AND khuyenmai.thoigianbatdau <= now() <= khuyenmai.thoigianketthuc
            $dm_text $search_text
            ORDER BY $sorted $limit";
$query = mysqli_query($link, $sql);
$sanpham = array();
while ($row = mysqli_fetch_array($query)) {
    $sanpham[] = $row;
}

// Get tổng số trang
$sql = "SELECT count(1) as number
            FROM sanpham
            $join JOIN khuyenmai ON khuyenmai.masanpham = sanpham.masanpham
                               AND khuyenmai.thoigianbatdau <= now() <= khuyenmai.thoigianketthuc
            $dm_text $search_text
            ORDER BY $sorted";
$query = mysqli_query($link, $sql);
$count = $query->fetch_assoc()["number"];
$count = intval(($count - 1) / 15 + 1, 0);

// Danh mục sản phẩm
$sql = "SELECT * FROM danhmucsanpham";
$query = mysqli_query($link, $sql);
$danhmucsanpham = array();
while ($row = mysqli_fetch_array($query)) {
    $danhmucsanpham[] = $row;
}
?>
    <div class="flex-wrap">
        <div class="side-menu">
            <div class="side-menu-header">
                <img width="10" src="images/menu.svg">
                <span>Tất cả danh mục</span>
            </div>
            <div class="side-menu-body">
                <?php foreach ($danhmucsanpham as $row) { ?>
                    <a href="sanpham.php?dm=<?= $row["madanhmuc"] ?>"
                       class="side-menu-link <?= $row["madanhmuc"] == $madanhmuc ? "active" : "" ?>">
                        <?= $row["tendanhmuc"] ?>
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="card transparent">
            <?php if ($search) { ?>
                <div class="search-result">Kết quả tìm kiếm cho "<strong><?= $search ?></strong>"</div>
            <?php } ?>
            <div class="sort-bar">
                <span class="sort-label">Sắp xếp theo</span>
                <a class="sort-item <?= $sort == "new" ? "active" : "" ?>"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=new"> Mới nhất</a>
                <a class="sort-item <?= $sort == "sold" ? "active" : "" ?>"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=sold">Bán chạy</a>
                <a class="sort-item <?= $sort == "sale" ? "active" : "" ?>"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=sale">Khuyến mãi</a>
                <a class="sort-item <?= $sort == "low" ? "active" : "" ?>"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=low">Giá thấp đến cao</a>
                <a class="sort-item <?= $sort == "high" ? "active" : "" ?>"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=high">Giá cao đến thấp</a>
                <span class="page"><strong><?= $page ?></strong>/<?= $count ?></span>
                <a class="button-prev"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=<?= $sort ?>&page=<?= $page == 1 ? 1 : $page - 1 ?>"><</a>
                <a class="button-next"
                   href="sanpham.php?dm=<?= $madanhmuc ?>&sort=<?= $sort ?>&page=<?= $page == $count ? $count : $page + 1 ?>">></a>
            </div>
            <div class="products">
                <?php foreach ($sanpham as $row) { ?>
                    <div class="sanpham">
                        <a href="chitiet.php?id=<?= $row["masanpham"] ?>">
                            <?php if ($row["giatri"]) { ?>
                                <span class="khuyenmai">-<?= $row["giatri"] ?>%</span>
                            <?php } ?>
                            <div class="hinhanhsanpham" style="background-image:url('<?= $row["hinhanh"] ?>')"></div>
                            <div class="tensanpham"><?= $row["tensanpham"] ?></div>
                            <div class="giasanpham">
                                <img src="images/coin.png" width="16">
                                <?php if ($row["giatri"]) { ?>
                                    <b>₫<?= number_format($row["giakhuyenmai"]) ?></b>
                                    <s>₫<?= number_format($row["gia"], 0, 3, '.') ?></s>
                                <?php } else { ?>
                                    ₫<?= number_format($row["gia"], 0, 3, '.') ?>
                                <?php } ?>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php include "footer.php" ?>