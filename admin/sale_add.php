<?php
include "header.php";

if (isset($_POST["save"])) {
    $masanpham = $_POST["masanpham"];
    $giatri = $_POST["giatri"];
    $batdau = $_POST["thoigianbatdau"];
    $ketthuc = $_POST["thoigianketthuc"];
    if ($batdau > $ketthuc) {
        $alert = "Ngày bắt đầu phải nhỏ hơn ngày kết thúc!";
    } else {
        $sql = "INSERT INTO khuyenmai (
                    masanpham,
                    thoigianbatdau,
                    thoigianketthuc,
                    giatri
                ) VALUES (
                    '$masanpham',
                    '$batdau',
                    '$ketthuc',
                    '$giatri'
                )";
        if ($db->query($sql)) {
            header("location: sale_list.php");
        } else echo $sql;
    }
}

if (isset($_GET["id"])) {
    $masanpham = $_GET["id"];
    $sql = "select masanpham, tensanpham, gia from sanpham where masanpham='$masanpham'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: product_list.php");
    }
} else {
    header("Location: product_list.php");
}
?>

<form method="post">
    <div class="edit-page">
        <div class="edit-header">
            <div class="edit-header-title">Tạo khuyến mãi</div>
            <div class="edit-header-subtitle">Create sale</div>
        </div>
        <div class="edit-body">
            <div class="edit-body-left">
                <div class="input-with-label">
                    <div class="input-with-label-label">Mã sản phẩm</div>
                    <div class="input-with-label-content">
                        <input type="text" name="masanpham" class="input-with-status-input"
                               value="<?= $row["masanpham"] ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Tên sản phẩm</div>
                    <div class="input-with-label-content">
                        <input type="text" name="tendanhmuc" class="input-with-status-input"
                               value="<?= $row["tensanpham"] ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Giá</div>
                    <div class="input-with-label-content">
                        <input type="text" name="gia" class="input-with-status-input"
                               value="<?= number_format($row["gia"]) ?>" readonly>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Giảm giá (%)</div>
                    <div class="input-with-label-content">
                        <input type="number" name="giatri" class="input-with-status-input" max="100" min="0" autofocus
                               required>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Ngày bắt đầu</div>
                    <div class="input-with-label-content">
                        <input type="date" class="input-with-status-input" name="thoigianbatdau" required>
                    </div>
                </div>
                <div class="input-with-label">
                    <div class="input-with-label-label">Ngày kết thúc</div>
                    <div class="input-with-label-content">
                        <input type="date" class="input-with-status-input" name="thoigianketthuc" required>
                    </div>
                </div>
                <div class="edit-submit">
                    <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                    <a href="product_list.php" class="btn-cancel">Trở lại</a>
                </div>
            </div>
        </div>
    </div>
</form>

<?php include "footer.php" ?>
