<?php include "header.php";
$sql = "select binhluan.*, khachhang.email from binhluan
        left join khachhang on binhluan.makhachhang = khachhang.makhachhang
        order by binhluan.thoigian desc";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<div class="card flex-column">
    <div class="card-header">
        <div class="card-header-title">Bình luận sản phẩm</div>
    </div>
    <div class="card-body">
        <table class="admin-table">
            <tr class="admin-table-row">
                <td class="admin-table-header">Email</td>
                <td class="admin-table-header">Mã sản phẩm</td>
                <td class="admin-table-header">Thời gian</td>
                <td class="admin-table-header">Trạng thái</td>
                <td class="admin-table-header">Chức năng</td>
            </tr>
            <?php foreach ($list as $item) { ?>
                <tr class="admin-table-row">
                    <td class="admin-table-data"><?= $item["email"] ?></td>
                    <td class="admin-table-data"><?= $item["masanpham"] ?></td>
                    <td class="admin-table-data"><?= $item["thoigian"] ?></td>
                    <td class="admin-table-data">
                        <?php
                        switch ($item["trangthai"]) {
                            case 0:
                                echo '<span class="badge badge-success">Mới</span>';
                                break;
                            case 1:
                                echo '<span class="badge badge-dark">Đã xem</span>';
                                break;
                        }
                        ?></td>
                    <td class="admin-table-data">
                        <a target="_blank"
                           href="../chitiet.php?id=<?= $item["masanpham"] ?>&comment=<?= $item["mabinhluan"] ?>">
                            Chi tiết</a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<?php include "footer.php" ?>
