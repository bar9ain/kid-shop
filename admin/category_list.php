<?php include "header.php";

if (isset($_POST["delete"])) {
    $madanhmuc = $_POST["delete"];
    $sql = "DELETE FROM danhmucsanpham WHERE madanhmuc='$madanhmuc'";
    $db->query($sql);
    header("Location: category_list.php");
}

$sql = "SELECT * FROM danhmucsanpham ORDER BY madanhmuc";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>

    <div class="card flex-column">
        <div class="card-header">
            <div class="card-header-title">Quản lý danh mục</div>
            <div class="card-header-button">
                <a href="category_add.php" class="btn btn-primary">Thêm mới</a>
            </div>
        </div>
        <div class="card-body">
            <table class="admin-table">
                <tr class="admin-table-row">
                    <th class="admin-table-header">Mã danh mục</th>
                    <th class="admin-table-header">Tên danh mục</th>
                    <th class="admin-table-header">Chức năng</th>
                </tr>
                <?php foreach ($list as $item) { ?>
                    <tr class="admin-table-row">
                        <td class="admin-table-data"><?= $item["madanhmuc"] ?></td>
                        <td class="admin-table-data"><?= $item["tendanhmuc"] ?></td>
                        <td class="admin-table-data">
                            <form method="post">
                                <a href="category_edit.php?id=<?= $item["madanhmuc"] ?>" class="btn btn-default">Sửa</a>
                                <button class="btn btn-danger" name="delete" value="<?= $item["madanhmuc"] ?>">Xóa
                            </form>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>

<?php include "footer.php" ?>