<?php
include "header.php";
if (!isset($_SESSION["makhachhang"])) {
    header("location: dangnhap.php");
}

$makhachhang = $_SESSION["makhachhang"];

if (isset($_POST["save"])) {
    $_hoten = $_POST["hoten"];
    $_diachi = $_POST["diachi"];
    $db->begin_transaction();
    $db->autocommit(false);
    $sql = "UPDATE khachhang
            SET hoten = '$_hoten',
                diachi = '$_diachi'
            WHERE makhachhang = '$makhachhang'";
    if ($db->query($sql)) {
        if ($_FILES["avatar"]["size"] > 0) {
            $ext = pathinfo($_FILES["avatar"]["name"], PATHINFO_EXTENSION);
            $_avatar = "images/" . time() . "." . $ext;
            move_uploaded_file($_FILES["avatar"]["tmp_name"], $_avatar);
            $sql = "UPDATE khachhang SET avatar = '$_avatar' WHERE makhachhang = '$makhachhang'";
            if (!$db->query($sql)) {
                $db->rollback();
            }
        }
        $db->commit();
        $_SESSION["hoten"] = $_hoten;
        header("location: hoso.php");
    } else {
        $db->rollback();
    }
}

$sql = "SELECT
            makhachhang,
            hoten,
            diachi,
            sdt,
            avatar,
            email
        FROM khachhang
        WHERE makhachhang='$makhachhang'";
$query = $db->query($sql);
$row = $query->fetch_assoc();
if (isset($row)) {
    $hoten = $row["hoten"];
    $diachi = $row["diachi"];
    $sdt = $row["sdt"];
    $email = $row["email"];
    $avatar = $row["avatar"];
} else {
    header("location: dangnhap.php");
}
?>
<form method="post" enctype="multipart/form-data">
    <div class="user-page">
        <?php include "hoso_menu.php" ?>
        <div class="user-page-content">
            <div class="user-page-header">
                <div class="user-page-header-title">Hồ sơ của tôi</div>
                <div class="user-page-header-subtitle">Quản lý thông tin hồ sơ để bảo mật tài khoản</div>
            </div>
            <div class="user-page-profile">
                <div class="user-page-profile-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên đầy đủ</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" name="hoten" value="<?= $hoten ?>" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Email</div>
                        <div class="input-with-label-content"><?= $email ?></div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Số điện thoại</div>
                        <div class="input-with-label-content"><?= $sdt ?></div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Địa chỉ</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" name="diachi" value="<?= $diachi ?>" required>
                        </div>
                    </div>
                    <div class="user-page-submit">
                        <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                    </div>
                </div>
                <div class="user-page-profile-right">
                    <div class="avatar">
                        <div class="avatar-image">
                            <?php if ($avatar) { ?>
                                <img src="<?= $avatar ?>">
                            <?php } else { ?>
                                <img src="images/avatar.svg">
                            <?php } ?>
                        </div>
                        <input name="avatar" type="file" accept=".jpg,.jpeg,.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include "footer.php" ?>
