<?php include "header.php";

if (isset($_POST["delete"])) {
    $makhuyenmai = $_POST["delete"];
    $sql = "DELETE FROM khuyenmai WHERE makhuyenmai='$makhuyenmai'";
    $db->query($sql);
    header("location: sale_list");
}

$admin = $_SESSION["admin"];

$sql = "SELECT
            sanpham.masanpham,
            sanpham.tensanpham,
            sanpham.gia,
            sanpham.gia * (100 - khuyenmai.giatri) / 100 as giakhuyenmai,
            khuyenmai.makhuyenmai,
            khuyenmai.giatri,
            khuyenmai.thoigianbatdau,
            khuyenmai.thoigianketthuc,
            case when thoigianbatdau <= now() and now() <= thoigianketthuc then 'Đang khuyến mãi'
                 when now() < thoigianbatdau then 'Chưa bắt đầu'
                 else 'Đã kết thúc'
            end as trangthai
        FROM khuyenmai
        LEFT JOIN sanpham ON khuyenmai.masanpham = sanpham.masanpham";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_assoc()) {
    $list[] = $row;
}

?>
<div class="card flex-column">
    <div class="card-header">
        <div class="card-header-title">Quản lý khuyến mãi</div>
    </div>
    <div class="card-body">
        <table class="admin-table">
            <tr class="admin-table-row">
                <th class="admin-table-header">Mã sản phẩm</th>
                <th class="admin-table-header">Tên sản phẩm</th>
                <th class="admin-table-header">Giá</th>
                <th class="admin-table-header">Giảm giá</th>
                <th class="admin-table-header">Thời gian</th>
                <th class="admin-table-header">Trạng thái</th>
                <th class="admin-table-header">Chức năng</th>
            </tr>
            <?php foreach ($list as $item) { ?>
                <tr class="admin-table-row">
                    <td class="admin-table-data"><?= $item["masanpham"] ?></td>
                    <td class="admin-table-data"><?= $item["tensanpham"] ?></td>
                    <td class="admin-table-data">
                        <s><?= number_format($item["gia"]) ?>đ</s><?= number_format($item["giakhuyenmai"]) ?>đ
                    </td>
                    <td class="admin-table-data"><?= $item["giatri"] ?>%</td>
                    <td class="admin-table-data">
                        Từ <?= date_format(date_create($item["thoigianbatdau"]), "d/m/Y") ?>
                        đến <?= date_format(date_create($item["thoigianketthuc"]), "d/m/Y") ?></td>
                    <td class="admin-table-data"><?= $item["trangthai"] ?></td>
                    <td class="admin-table-data">
                        <form method="post">
                            <a href="sale_edit.php?id=<?= $item["makhuyenmai"] ?>" class="btn btn-default">Sửa</a>
                            <button class="btn btn-danger" name="delete" value="<?= $item["makhuyenmai"] ?>">Xóa
                            </button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<?php include "footer.php" ?>
