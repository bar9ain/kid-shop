<?php
if (session_status() === PHP_SESSION_NONE)
    session_start();
include "../csdl.php";

if (!isset($_SESSION["admin"])) {
    header("Location: index.php");
}

// Lấy số comment mới
$sql = "select count(1) as number from binhluan where trangthai = 0";
$query = $db->query($sql);
$comments = $query->fetch_assoc()["number"];

?>

<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <link href="../css/bootstrap.min.css" rel=" stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    <link rel="icon" href="../images/coin.png">
</head>

<body>
<div class="admin-page-menu">
    <ul class="admin-menu">
        <li class="admin-menu-item"><a href="admin_list.php">Quản trị viên</a></li>
        <li class="admin-menu-item"><a href="category_list.php">Danh mục</a></li>
        <li class="admin-menu-item"><a href="product_list.php">Sản phẩm</a></li>
        <li class="admin-menu-item"><a href="sale_list.php">Khuyến mãi</a></li>
        <li class="admin-menu-item"><a href="order_list.php">Đơn hàng</a></li>
        <li class="admin-menu-item"><a href="comments.php">Bình luận (<?= $comments ?>)</a></li>
        <li class="admin-menu-item"><a href="customer_list.php">Khách hàng</a></li>
        <li class="admin-menu-item"><a href="password.php">Đổi mật khẩu</a></li>
        <li class="admin-menu-item"><a href="logout.php">Đăng xuất</a></li>
    </ul>
</div>

<div class="page-content">
