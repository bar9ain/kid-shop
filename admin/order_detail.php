<?php
include "header.php";

if (isset($_POST["update"])) {
    $madonhang = $_POST["update"];
    $trangthai = $_POST["trangthai"];
    $sql = "UPDATE donhang
            SET trangthai = $trangthai
            WHERE madonhang = '$madonhang'";
    if ($db->query($sql)) {
        header("location: order_list.php");
    } else echo $sql;
}

if (isset($_GET["id"])) {
    $madonhang = $_GET["id"];
    $sql = "SELECT
            donhang.madonhang,
            donhang.tongtien,
            donhang.ngaythem,
            donhang.trangthai,
            khachhang.hoten,
            khachhang.diachi,
            khachhang.sdt,
            khachhang.email
        FROM donhang
        LEFT JOIN khachhang ON donhang.makhachhang = khachhang.makhachhang
        WHERE madonhang = '$madonhang'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: order_list.php");
    }
    $sql = "SELECT
                sanpham.masanpham,
                sanpham.hinhanh,
                chitietdonhang.tensanpham,
                chitietdonhang.dongia,
                chitietdonhang.soluong
            FROM chitietdonhang
            LEFT JOIN sanpham ON chitietdonhang.masanpham = sanpham.masanpham
            WHERE madonhang = '$madonhang'";
    $result = $db->query($sql);
    $list = array();
    while ($i = $result->fetch_array()) {
        $list[] = $i;
    }
} else {
    header("Location: order_list.php");
}
?>

<form method="post">
    <div class="edit-page">
        <div class="edit-header">
            <div class="edit-header-title">Chi tiết đơn hàng</div>
            <div class="edit-header-subtitle">Mã đơn hàng: <?= $madonhang ?> | Thời gian: <?= $row["ngaythem"] ?></div>
        </div>
        <div class="edit-body">
            <div class="edit-body-left">
                <div class="cart-detail-customer"><?= $row["hoten"] ?></div>
                <div class="cart-detail-description"><?= $row["diachi"] ?></div>
                <div class="cart-detail-description"><?= $row["sdt"] ?></div>
                <div class="cart-detail-description"><?= $row["email"] ?></div>
            </div>
            <div class="edit-body-cart">
                <?php foreach ($list as $item) { ?>
                    <div class="cart-detail-row">
                        <div class="cart-detail-image">
                            <img src="../<?= $item["hinhanh"] ?>">
                        </div>
                        <div class="cart-detail-text">
                            <div class="cart-detail-title"><?= $item["tensanpham"] ?></div>
                            <div class="cart-detail-quantity">x<?= $item["soluong"] ?></div>
                        </div>
                        <div class="cart-detail-price"><?= number_format($item["dongia"]) ?>đ</div>
                    </div>
                <?php } ?>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Tổng tiền hàng</div>
                    <div class="cart-detail-summary-value">
                        <span><?= number_format($row["tongtien"]) ?>đ</span>
                    </div>
                </div>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Trạng thái</div>
                    <div class="cart-detail-summary-value">
                        <select class="input-with-status-input" name="trangthai">
                            <option value="0" <?= $row["trangthai"] == 0 ? "selected" : "" ?>>Mới</option>
                            <option value="1" <?= $row["trangthai"] == 1 ? "selected" : "" ?>>Đã xác nhận</option>
                            <option value="2" <?= $row["trangthai"] == 2 ? "selected" : "" ?>>Đã giao hàng</option>
                            <option value="3" <?= $row["trangthai"] == 3 ? "selected" : "" ?>>Đã nhận hàng</option>
                            <option value="4" <?= $row["trangthai"] == 4 ? "selected" : "" ?>>Đã hủy</option>
                        </select>
                    </div>
                </div>
                <div class="edit-submit">
                    <a href="order_list.php" class="btn-cancel">Trở lại</a>
                    <button class="btn btn-submit" name="update" value="<?= $row["madonhang"] ?>">Cập nhật</button>
                </div>
            </div>
        </div>
    </div>
</form>

<?php include "footer.php" ?>
