<?php include "header.php";

if (isset($_SESSION["makhachhang"])) {
    header("Location: index.php");
}

if (isset($_POST["login"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $matkhau = $_POST["matkhau"];
    $sql = "SELECT makhachhang, hoten
            FROM khachhang
            WHERE (email='$tendangnhap' OR sdt='$tendangnhap')
            AND password='$matkhau'";
    $check = mysqli_query($link, $sql);
    if (mysqli_num_rows($check) > 0) {
        $row = mysqli_fetch_assoc($check);
        $_SESSION["makhachhang"] = $row["makhachhang"];
        $_SESSION["hoten"] = $row["hoten"];
        header("Location: index.php");
    } else {
        $login_error = true;
    }
}

?>
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Đăng nhập</a>
                <a class="authen-header-tab" href="dangky.php">Đăng ký</a>
            </div>
            <div class="authen-body">
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="tendangnhap"
                           placeholder="Email/Số điện thoại"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="password"
                           name="matkhau"
                           placeholder="Mật khẩu"
                           required>
                </div>
                <?php if (isset($login_error)) { ?>
                    <div class="authen-error">Tài khoản hoặc mật khẩu không đúng</div>
                <?php } ?>
                <div class="forgot-password">
                    <a href="quenmatkhau.php">Quên mật khẩu</a>
                </div>
            </div>
            <div class="authen-footer">
                <a href="index.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="login" class="btn btn-submit">
                    Đăng nhập
                </button>
            </div>
        </div>
    </form>
<?php include "footer.php" ?>