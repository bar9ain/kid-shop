<?php
if (session_status() === PHP_SESSION_NONE)
    session_start();
include "csdl.php";

if (!isset($title))
    $title = "Toys Shop";

// Danh mục sản phẩm
$sql = "SELECT * FROM danhmucsanpham WHERE hienthimenu = 1";
$query = mysqli_query($link, $sql);
$danhmucsanpham = array();
while ($row = mysqli_fetch_array($query)) {
    $danhmucsanpham[] = $row;
}

?>

<head>
    <title><?= $title ?></title>
    <meta charset="utf-8">
    <link href=" css/bootstrap.min.css" rel=" stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="icon" href="images/coin.png">
    <script src="js/jquery-1.9.1.min.js"></script>
    <script src=" js/bootstrap.min.js"></script>
</head>

<body>
<div class="page-header">
    <div class="wrapper">
        <div class="page-user">
            <?php if (isset($_SESSION["makhachhang"])) { ?>
                Xin chào, <a href="hoso.php"><?= $_SESSION["hoten"] ?></a>  |
                <a href="logout.php">Đăng xuất</a>
            <?php } else { ?>
                <a href="dangnhap.php">Đăng nhập</a>
                | <a href="dangky.php">Đăng ký</a>
            <?php } ?>
        </div>
        <div class="page-logo">
            <a href="index.php"></a>
        </div>
        <form class="page-search" action="sanpham.php" method="get">
            <input name="search" placeholder="Tìm kiếm...">
            <button class="button" type="submit">
                <svg class="svg-icon" height="19" viewBox="0 0 19 19" width="19">
                    <g fill="white" transform="translate(-1016 -32)">
                        <g transform="translate(405 21)">
                            <g transform="translate(611 11)">
                                <path d="m8 16c4.418278 0 8-3.581722 8-8s-3.581722-8-8-8-8 3.581722-8 8 3.581722 8 8 8zm0-2c-3.3137085 0-6-2.6862915-6-6s2.6862915-6 6-6 6 2.6862915 6 6-2.6862915 6-6 6z"></path>
                                <path d="m12.2972351 13.7114222 4.9799555 4.919354c.3929077.3881263 1.0260608.3842503 1.4141871-.0086574.3881263-.3929076.3842503-1.0260607-.0086574-1.414187l-4.9799554-4.919354c-.3929077-.3881263-1.0260608-.3842503-1.4141871.0086573-.3881263.3929077-.3842503 1.0260608.0086573 1.4141871z"></path>
                            </g>
                        </g>
                    </g>
                </svg>
            </button>
        </form>
        <div class="page-cart">
            <a href="giohang.php">
                <img alt="cart" src="images/cart.png">
                <?php if (isset($_SESSION["giohang"]) && count($_SESSION["giohang"]) > 0) { ?>
                    <div class="cart-count">
                        <?= count($_SESSION["giohang"]) ?>
                    </div>
                <?php } ?>
            </a>
        </div>
    </div>
</div>

<div class="menu">
    <div class="wrapper">
        <ul>
            <li><a href="index.php">Trang chủ</a></li>
            <li>
                <a>Danh mục</a>
                <ul class="menu-dropdown">
                    <?php foreach ($danhmucsanpham as $row) { ?>
                        <li><a href="sanpham.php?dm=<?= $row["madanhmuc"] ?>"><?= $row["tendanhmuc"] ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li><a href="sanpham.php?sort=sale">Khuyến mãi</a></li>
            <li><a href="sanpham.php?sort=new">Đồ chơi mới</a></li>
            <li><a href="sanpham.php?sort=sold">Đồ chơi bán chạy</a></li>
        </ul>
    </div>
</div>

<div class="page-content">