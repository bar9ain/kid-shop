<?php include "header.php";

if (isset($_POST["reset"])) {
    $tendangnhap = $_POST["reset"];
    $sql = "UPDATE admin 
            SET matkhau = '1',
                reset = 0
            WHERE tendangnhap = '$tendangnhap'";
    if ($db->query($sql)) {
        $alert = "Mật khẩu của $tendangnhap đã được đặt lại thành 1";
    }
}

if (isset($_POST["delete"])) {
    $tendangnhap = $_POST["delete"];
    $sql = "DELETE FROM admin WHERE tendangnhap='$tendangnhap'";
    $db->query($sql);
    header("location: admin_list.php");
}

$admin = $_SESSION["admin"];

$sql = "SELECT tendangnhap, reset
        FROM admin
        WHERE tendangnhap != '$admin'";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_assoc()) {
    $list[] = $row;
}

?>
<div class="card flex-column">
    <div class="card-header">
        <div class="card-header-title">Quản lý quản trị viên</div>
        <div class="card-header-button">
            <a href="admin_add.php" class="btn btn-primary">Thêm mới</a>
        </div>
    </div>
    <div class="card-body">
        <table class="admin-table">
            <tr class="admin-table-row">
                <th class="admin-table-header">Tên tài khoản</th>
                <th class="admin-table-header">Ghi chú</th>
                <th class="admin-table-header">Chức năng</th>
            </tr>
            <?php foreach ($list as $item) { ?>
                <tr class="admin-table-row">
                    <td class="admin-table-data"><?= $item["tendangnhap"] ?></td>
                    <td class="admin-table-data"><?= $item["reset"] ? "Quên mật khẩu" : "" ?></td>
                    <td class="admin-table-data">
                        <form method="post">
                            <button class="btn btn-danger" name="delete" value="<?= $item["tendangnhap"] ?>">Xóa</button>
                            <button class="btn btn-primary" name="reset" value="<?= $item["tendangnhap"] ?>">Đặt lại mật khẩu</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
<?php include "footer.php" ?>
