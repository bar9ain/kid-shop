<?php include "header.php";

if (isset($_POST["madanhmuc"])) {
    $madanhmuc = $_POST["madanhmuc"];
    $tendanhmuc = $_POST["tendanhmuc"];
    $hienthitrangchu = isset($_POST["hienthitrangchu"]) ? 1 : 0;
    $hienthimenu = isset($_POST["hienthimenu"]) ? 1 : 0;
    if (isset($_FILES["hinhanh"]) && $_FILES["hinhanh"]["size"] > 0) {
        $time = time();
        $type = explode("/", $_FILES["hinhanh"]["type"])[1];
        $hinhanh = "images/$time.$type";
        move_uploaded_file($_FILES["hinhanh"]["tmp_name"], "../" . $hinhanh);
        $sql = "update danhmucsanpham 
                set tendanhmuc='$tendanhmuc',
                    hienthitrangchu = $hienthitrangchu,
                    hienthimenu = $hienthimenu,
                    hinhanhdanhmuc = '$hinhanh'
                where madanhmuc='$madanhmuc'";
    } else {
        $sql = "update danhmucsanpham 
                set tendanhmuc='$tendanhmuc',
                    hienthitrangchu = $hienthitrangchu,
                    hienthimenu = $hienthimenu
                where madanhmuc='$madanhmuc'";
    }
    if ($db->query($sql)) {
        header("Location: category_list.php");
    } else {
        echo $sql;
    }
}
if (isset($_GET["id"])) {
    $madanhmuc = $_GET["id"];
    $sql = "select * from danhmucsanpham where madanhmuc='$madanhmuc'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null)
        header("Location: category_list.php");
} else {
    header("Location: category_list.php");
}

?>
    <form method="post" enctype="multipart/form-data">
        <div class="edit-page">
            <div class="edit-header">
                <div class="edit-header-title">Chỉnh sửa danh mục</div>
                <div class="edit-header-subtitle">Edit category</div>
            </div>
            <div class="edit-body">
                <div class="edit-body-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Mã danh mục</div>
                        <div class="input-with-label-content">
                            <input type="text" name="madanhmuc" class="input-with-status-input" value="<?= $row["madanhmuc"] ?>"
                                   required readonly>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên danh mục</div>
                        <div class="input-with-label-content">
                            <input type="text" name="tendanhmuc" class="input-with-status-input" value="<?= $row["tendanhmuc"] ?>"
                                   required autofocus>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Hiển thị ở trang chủ</div>
                        <div class="input-with-label-content">
                            <input id="homepage" type="checkbox"
                                   name="hienthitrangchu" <?= $row["hienthitrangchu"] == 1 ? "checked" : "" ?>>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Hiển thị ở menu</div>
                        <div class="input-with-label-content">
                            <input id="menu" type="checkbox"
                                   name="hienthimenu" <?= $row["hienthimenu"] == 1 ? "checked" : "" ?>>
                        </div>
                    </div>
                    <div class="edit-submit">
                        <button type="submit" name="save" class="btn btn-submit">Lưu</button>
                        <a href="category_list.php" class="btn-cancel">Trở lại</a>
                    </div>
                </div>
                <div class="edit-body-right">
                    <div class="edit-thumbnail">
                        <div class="edit-thumbnail-image">
                            <img src="../<?= $row["hinhanhdanhmuc"] ?>">
                        </div>
                        <input type="file" accept="image/*" name="hinhanh">
                    </div>
                </div>
            </div>
        </div>
    </form>
<?php include "footer.php"; ?>