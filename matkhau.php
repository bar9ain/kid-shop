<?php
include "header.php";
if (!isset($_SESSION["makhachhang"])) {
    header("location: dangnhap.php");
}

$makhachhang = $_SESSION["makhachhang"];

if (isset($_POST["update_password"])) {
    $matkhaucu = $_POST["matkhaucu"];
    $matkhau = $_POST["matkhau"];
    $matkhau2 = $_POST["matkhau2"];
    if ($matkhau !== $matkhau2) {
        $alert = "Mật khẩu và Mật khẩu xác nhận không giống nhau";
    } else {
        $sql = "SELECT password
                FROM khachhang
                WHERE makhachhang = '$makhachhang'";
        $query = $db->query($sql);
        $result = $query->fetch_assoc();
        if ($matkhaucu !== $result["password"]) {
            $alert = "Mật khẩu cũ không chính xác";
        } else {
            $sql = "UPDATE khachhang
                    SET password = '$matkhau'
                    WHERE makhachhang = '$makhachhang'";
            if ($db->query($sql)) {
                $alert = "Mật khẩu mới đã được cập nhật!";
            }
        }
    }
}
?>
<form method="post" enctype="multipart/form-data">
    <div class="user-page">
        <?php include "hoso_menu.php" ?>
        <div class="user-page-content">
            <div class="user-page-header">
                <div class="user-page-header-title">Đổi mật khẩu</div>
                <div class="user-page-header-subtitle">Để bảo mật tài khoản, vui lòng không chia sẻ mật khẩu cho người
                    khác
                </div>
            </div>
            <div class="user-page-profile">
                <div class="user-page-profile-password">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Mật khẩu hiện tại</div>
                        <div class="input-with-label-content">
                            <input type="password" class="input-with-status-input" name="matkhaucu" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Mật khẩu mới</div>
                        <div class="input-with-label-content">
                            <input type="password" class="input-with-status-input" name="matkhau" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Xác nhận mật khẩu</div>
                        <div class="input-with-label-content">
                            <input type="password" class="input-with-status-input" name="matkhau2" required>
                        </div>
                    </div>
                    <div class="user-page-submit">
                        <button type="submit" name="update_password" class="btn btn-submit">Xác nhận</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include "footer.php" ?>
