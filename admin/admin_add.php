<?php include "header.php";
if (isset($_POST["admin_add"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $matkhau = $_POST["matkhau"];
    $matkhau2 = $_POST["matkhau2"];
    if ($matkhau !== $matkhau2) {
        $register_error = "Mật khẩu không khớp!";
    } else {
        $sql = "INSERT INTO admin(
                    tendangnhap,
                    matkhau
                ) VALUES (
                    '$tendangnhap',
                    '$matkhau'
                )";
        if ($db->query($sql)) {
            header("location: admin_list.php");
        } else {
            $register_error = "Tên tài khoản đã tồn tại";
        }
    }
}
?>
<form method="post">
    <div class="authen-modal">
        <div class="authen-header">
            <a class="authen-header-tab active">Tạo tài khoản</a>
        </div>
        <div class="authen-body">
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="text"
                       name="tendangnhap"
                       placeholder="Tên tài khoản"
                       autofocus
                       required>
            </div>
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="password"
                       name="matkhau"
                       placeholder="Mật khẩu"
                       required>
            </div>
            <div class="input-with-status">
                <input class="input-with-status-input"
                       type="password"
                       name="matkhau2"
                       placeholder="Xác nhận mật khẩu"
                       required>
            </div>
            <?php if (isset($register_error)) { ?>
                <div class="authen-error"><?= $register_error ?></div>
            <?php } ?>
        </div>
        <div class="authen-footer">
            <a href="admin_list.php" class="btn-cancel">Trở Lại</a>
            <button type="submit" name="admin_add" class="btn btn-submit">
                Thêm
            </button>
        </div>
    </div>
</form>
<?php include "footer.php" ?>
