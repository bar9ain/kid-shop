<?php
$sql = "SELECT
            hoten,
            sdt,
            avatar
        FROM khachhang
        WHERE makhachhang='$makhachhang'";
$query = $db->query($sql);
$row = $query->fetch_assoc();
$__hoten = $row["hoten"];
$__sdt = $row["sdt"];
$__avatar = $row["avatar"];
?>
<div class="user-page-sidebar">
    <div class="user-page-brief">
        <div class="user-page-brief-avatar">
            <?php if ($__avatar) { ?>
                <img src="<?= $__avatar ?>">
            <?php } else { ?>
                <img src="images/avatar.svg">
            <?php } ?>
        </div>
        <div class="user-page-brief-right">
            <div class="user-page-brief-username"><?= $__hoten ?></div>
            <div class="user-page-brief-phone"><?= $__sdt ?></div>
        </div>
    </div>
    <div class="user-page-sidebar-menu">
        <a class="user-page-sidebar-menu-entry" href="hoso.php">Thông tin cá nhân</a>
        <a class="user-page-sidebar-menu-entry" href="matkhau.php">Đổi mật khẩu</a>
        <a class="user-page-sidebar-menu-entry" href="donhang.php">Đơn hàng</a>
        <a class="user-page-sidebar-menu-entry" href="logout.php">Đăng xuất</a>
    </div>
</div>