<?php
session_start();
if (isset($_POST["giohang"]) || isset($_POST["muangay"])) {
    if ($_POST["soluong"] > $_POST["soluongton"]) {
        $alert = "Số lượng còn lại không đủ để đặt!";
        return;
    }
    $giohang = isset($_SESSION["giohang"]) ? $_SESSION["giohang"] : array();
    $daCoTrongGioHang = false;
    foreach ($giohang as $item) {
        if ($item->masanpham === $_POST["masanpham"]) {
            $item->soluong += $_POST["soluong"];
            $daCoTrongGioHang = true;
            break;
        }
    }
    if (!$daCoTrongGioHang) {
        $giohang[] = (object)[
            "masanpham" => $_POST["masanpham"],
            "tensanpham" => $_POST["tensanpham"],
            "hinhanh" => $_POST["hinhanh"],
            "gia" => isset($_POST["giakhuyenmai"]) ? $_POST["giakhuyenmai"] : $_POST["gia"],
            "soluong" => $_POST["soluong"]
        ];
    }
    $_SESSION["giohang"] = $giohang;
    if (isset($_POST["muangay"]))
        header("location: giohang.php");
}