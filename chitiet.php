<?php
$title = "Chi tiết sản phẩm";
include "dathang.php";
include "header.php";

if (isset($_GET["id"])) {
    // Nội dung sản phẩm
    $masanpham = $_GET["id"];

    // Cập nhật trạng thái đã xem cuả bình luận
    if (isset($_GET["comment"])) {
        $comment_id = $_GET["comment"];
        $sql = "UPDATE binhluan SET trangthai = 1 WHERE mabinhluan = '$comment_id'";
        if ($db->query($sql)) {
            header("Location: chitiet.php?id=$masanpham#comment_$comment_id");
        }
    }

    // Gửi comment
    if (isset($_POST["comment"])) {
        $customer_id = $_SESSION["makhachhang"];
        $comment = $_POST["comment"];
        echo $comment;
        $sql = "INSERT INTO binhluan (makhachhang, masanpham, noidung)
                VALUES ('$customer_id', '$masanpham', '$comment')";
        if ($db->query($sql)) {
            header("Location: chitiet.php?id=$masanpham#comments");
        } else echo $db->error;
        echo $sql;
    }

    // Cập nhật lượt xem
    $sql = "UPDATE sanpham
            SET luotxem = luotxem + 1
            WHERE masanpham = '$masanpham'";
    mysqli_query($link, $sql);

    $sql = "SELECT sanpham.*,
                   danhmucsanpham.tendanhmuc,
                   khuyenmai.giatri,
                   khuyenmai.thoigianbatdau,
                   khuyenmai.thoigianketthuc,
                   sanpham.gia * (100 - khuyenmai.giatri) / 100 as giakhuyenmai
            FROM sanpham
            LEFT JOIN danhmucsanpham ON sanpham.madanhmuc = danhmucsanpham.madanhmuc
            LEFT JOIN khuyenmai ON khuyenmai.masanpham = sanpham.masanpham
                               AND khuyenmai.thoigianbatdau <= now() <= khuyenmai.thoigianketthuc
            WHERE sanpham.masanpham = '$masanpham'";
    $query = mysqli_query($link, $sql);
    $item = mysqli_fetch_assoc($query);
    if ($item === null)
        header("Location: index.php");

    // Sản phẩm khác
    $sql = "SELECT
                sanpham.masanpham,
                sanpham.tensanpham,
                sanpham.hinhanh,
                sanpham.gia,
                khuyenmai.giatri,
                sanpham.gia * (100 - khuyenmai.giatri) / 100 as giakhuyenmai
            FROM sanpham
            LEFT JOIN khuyenmai ON khuyenmai.masanpham = sanpham.masanpham
                               AND khuyenmai.thoigianbatdau <= now() <= khuyenmai.thoigianketthuc
            WHERE sanpham.masanpham != $masanpham
            ORDER BY sanpham.luotxem DESC
            LIMIT 0, 6";
    $query = mysqli_query($link, $sql);
    $sanphamkhac = array();
    while ($row = mysqli_fetch_array($query))
        $sanphamkhac[] = $row;

    // Danh sách bình luận
    $sql = "select * from binhluan
            left join khachhang on khachhang.makhachhang = binhluan.makhachhang
            where masanpham = $masanpham
            order by binhluan.thoigian desc";
    $query = $db->query($sql);
    $comments = array();
    while ($row = $query->fetch_array())
        $comments[] = $row;
} else {
    header("Location: index.php");
}
?>

    <form method="post">
        <input type="hidden" name="masanpham" value="<?= $item["masanpham"] ?>">
        <input type="hidden" name="tensanpham" value="<?= $item["tensanpham"] ?>">
        <input type="hidden" name="hinhanh" value="<?= $item["hinhanh"] ?>">
        <input type="hidden" name="gia" value="<?= $item["gia"] ?>">
        <input type="hidden" name="soluongton" value="<?= $item["soluong"] ?>">
        <div class="card">
            <div class="hinhanhsanpham">
                <img width="150" src="<?= $item["hinhanh"] ?>">
            </div>
            <div class="motasanpham">
                <div class="tensanpham"><?= $item["tensanpham"] ?></div>
                <div class="giasanpham">
                    <?php if ($item["giatri"]) { ?>
                        <s>₫<?= number_format($item["gia"], 0, 3, '.') ?></s>
                        ₫<?= number_format($item["giakhuyenmai"]) ?>
                        <span class="giamgia">Giảm <?= $item["giatri"] ?>%</span>
                        <p class="ngaykhuyenmai">
                            Từ <?= date_format(date_create($item["thoigianbatdau"]), "d/m/Y") ?>
                            đến <?= date_format(date_create($item["thoigianketthuc"]), "d/m/Y") ?>
                        </p>
                        <input type="hidden" name="giakhuyenmai" value="<?= $item["giakhuyenmai"] ?>">
                    <?php } else { ?>
                        ₫<?= number_format($item["gia"], 0, 3, '.') ?>
                    <?php } ?>
                </div>
                <div class="chitiet">
                    <div class="title">Vận chuyển</div>
                    <div class="content">
                        <img src="images/mienphi.png" width="25" height="15">
                        &nbsp;Miễn phí vận chuyển
                    </div>
                </div>
                <div class="chitiet">
                    <div class="title">Số lượng</div>
                    <div class="content">
                        <div class="nhapsoluong">
                            <button class="button-outline" type="button" id="btn-minus">
                                <svg class="svg-icon" enable-background="new 0 0 10 10" viewBox="0 0 10 10" x="0" y="0">
                                    <polygon
                                            points="4.5 4.5 3.5 4.5 0 4.5 0 5.5 3.5 5.5 4.5 5.5 10 5.5 10 4.5"></polygon>
                                </svg>
                            </button>
                            <input class="input-outline" type="number" value="1" min="1" name="soluong">
                            <button class="button-outline" type="button" id="btn-plus">
                                <svg class="svg-icon" enable-background="new 0 0 10 10" viewBox="0 0 10 10"
                                     x="0" y="0">
                                    <polygon
                                            points="10 4.5 5.5 4.5 5.5 0 4.5 0 4.5 4.5 0 4.5 0 5.5 4.5 5.5 4.5 10 5.5 10 5.5 5.5 10 5.5"></polygon>
                                </svg>
                            </button>
                        </div>
                        <span id="count"><?= $item["soluong"] ?></span>&nbsp;sản phẩm có sẵn
                    </div>
                </div>
                <div class="muahang">
                    <button class="button invert" name="giohang">Thêm vào giỏ hàng</button>
                    <button class="button" name="muangay">Mua ngay</button>
                </div>
                <div class="quangcao">
                    <div>
                        <img src="images/doitra.png">
                        <span>7 ngày miễn phí trả hàng</span>
                    </div>
                    <div>
                        <img src="images/chinhhang.png">
                        <span>Hàng chính hãng 100%</span>
                    </div>
                    <div>
                        <img src="images/vanchuyen.png">
                        <span>Miễn phí vận chuyển</span>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <div class="card">
        <div class="chitietsanpham">
            <div class="tieude">Chi tiết sản phẩm</div>
            <div class="noidung">
                <div class="chitiet">
                    <div class="title">Danh mục</div>
                    <div class="content">
                        <a href="sanpham.php?dm=<?= $item["madanhmuc"] ?>">
                            <?= $item["tendanhmuc"] ?>
                        </a></div>
                </div>
                <div class="chitiet">
                    <div class="title">Thương hiệu</div>
                    <div class="content"><?= $item["thuonghieu"] ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Chất liệu</div>
                    <div class="content"><?= $item["chatlieu"] ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Xuất xứ</div>
                    <div class="content"><?= $item["xuatxu"] ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Kho</div>
                    <div class="content"><?= $item["soluong"] ?></div>
                </div>
                <div class="chitiet">
                    <div class="title">Đã bán</div>
                    <div class="content"><?= $item["luotmua"] ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="chitietsanpham" id="comments">Bình luận</div>
        <?php if (isset($_SESSION["makhachhang"])) { ?>
            <div class="comment-box">
                <form method="post">
                    <input name="comment" class="comment-input"
                           placeholder="Bình luận với tên <?= $_SESSION["hoten"] ?>..." required>
                    <button class="button">Gửi</button>
                </form>
            </div>
        <?php }
        foreach ($comments as $row) { ?>
            <div class="comment" id="comment_<?= $row["mabinhluan"] ?>">
                <div class="comment-profile">
                    <div class="comment-avatar">
                        <?php if ($row["avatar"]) { ?>
                            <img src="<?= $row["avatar"] ?>">
                        <?php } else { ?>
                            <img src="images/avatar.svg">
                        <?php } ?>
                    </div>
                    <div class="comment-name">
                        <div class="comment-fullname"><?= $row["hoten"] ?></div>
                        <div class="comment-time"><?= $row["thoigian"] ?></div>
                    </div>
                </div>
                <div class="comment-text"><?= $row["noidung"] ?></div>
            </div>
        <?php } ?>
    </div>

    <div class="card transparent">
        <div class="tieudedanhmuc">Các sản phẩm khác</div>
        <?php foreach ($sanphamkhac as $row) { ?>
            <div class="sanpham">
                <a href="chitiet.php?id=<?= $row["masanpham"] ?>">
                    <?php if ($row["giatri"]) { ?>
                        <span class="khuyenmai">-<?= $row["giatri"] ?>%</span>
                    <?php } ?>
                    <div class="hinhanhsanpham" style="background-image:url('<?= $row["hinhanh"] ?>')"></div>
                    <div class="tensanpham"><?= $row["tensanpham"] ?></div>
                    <div class="giasanpham">
                        <img src="images/coin.png" width="16">
                        <?php if ($row["giatri"]) { ?>
                            <b>₫<?= number_format($row["giakhuyenmai"]) ?></b>
                            <s>₫<?= number_format($row["gia"], 0, 3, '.') ?></s>
                        <?php } else { ?>
                            ₫<?= number_format($row["gia"], 0, 3, '.') ?>
                        <?php } ?>
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

<?php include "footer.php" ?>