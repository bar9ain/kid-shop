<?php
include "header.php";
if (!isset($_SESSION["makhachhang"])) {
    header("location: dangnhap.php");
}

$makhachhang = $_SESSION["makhachhang"];

$sql = "SELECT *
        FROM donhang
        WHERE makhachhang='$makhachhang'";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<div class="user-page">
    <?php include "hoso_menu.php" ?>
    <div class="user-page-content">
        <div class="user-page-header">
            <div class="user-page-header-title">Đơn hàng của tôi</div>
            <div class="user-page-header-subtitle">Lịch sử các đơn hàng mà bạn đã đặt</div>
        </div>
        <div class="user-page-profile">
            <div class="cart-history">
                <div class="cart-detail-row">
                    <div class="cart-detail-text"><b>Mã đơn hàng</b></div>
                    <div class="cart-detail-text"><b>Thời gian</b></div>
                    <div class="cart-detail-text"><b>Tổng tiền</b></div>
                    <div class="cart-detail-text"><b>Trạng thái</b></div>
                    <div class="cart-detail-text"><b>Chi tiết</b></div>
                </div>
                <?php foreach ($list as $item) { ?>
                    <div class="cart-detail-row">
                        <div class="cart-detail-text"><?= $item["madonhang"] ?></div>
                        <div class="cart-detail-text"><?= $item["ngaythem"] ?></div>
                        <div class="cart-detail-text"><?= number_format($item["tongtien"]) ?>đ</div>
                        <div class="cart-detail-text">
                            <?php
                            switch ($item["trangthai"]) {
                                case 0:
                                    echo "Mới";
                                    break;
                                case 1:
                                    echo "Đã xác nhận";
                                    break;
                                case 2:
                                    echo "Đã giao hàng";
                                    break;
                                case 3:
                                    echo "Đã nhận hàng";
                                    break;
                                case 4:
                                    echo "Đã hủy";
                                    break;
                            }
                            ?>
                        </div>
                        <div class="cart-detail-text">
                            <a href="donhang_chitiet.php?id=<?= $item["madonhang"] ?>">Chi tiết</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
