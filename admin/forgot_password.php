<?php include "../csdl.php";
if (session_status() === PHP_SESSION_NONE)
    session_start();

if (isset($_POST["forgot_password"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $sql = "UPDATE admin SET reset = 1 WHERE tendangnhap = '$tendangnhap'";
    if ($db->query($sql)) {
        $alert = "Thông tin đã được gửi đến hệ thống, vui lòng đợi!";
        $redirect = "index.php";
    }
}
?>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <link href="../css/bootstrap.min.css" rel=" stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../css/style.css"/>
    <link rel="icon" href="../images/coin.png">
</head>

<body>
<div class="page-content">
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Quên mật khẩu</a>
            </div>
            <div class="authen-body">
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="tendangnhap"
                           placeholder="Tên tài khoản"
                           autofocus
                           required>
                </div>
            </div>
            <div class="authen-footer">
                <a href="index.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="forgot_password" class="btn btn-submit">
                    Xác nhận
                </button>
            </div>
        </div>
    </form>
</div>
</body>
<?php include "footer.php" ?>
